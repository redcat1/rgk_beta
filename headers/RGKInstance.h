﻿// Instance.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание класса RGK::Instance
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "decl.h"
#include "forward.h"
#include "Common/FlagSet.h"
#include "Common/Version.h"
#include "Common/String.h"
#include <string>

namespace RGK
{
#ifdef ENGLISH_HELP
    /// <summary>Kernel Initialization</summary>
    /// A single object of the class is created automatically for the whole application (process).
    /// It contains system-wide kernel settings. All of the class methods are static.
#else
    /// <summary>Инициализация ядра</summary>
    /// Класс автоматически создаётся в одном экземпляре на всё приложение (процесс) 
    /// и хранит общесистемные настройки ядра. Все методы класса являются статическими.
#endif
    class DLLEXPORT Instance
    {
    public:
#ifdef ENGLISH_HELP
        /// Start the kernel.
        /// If the kernel hasn't been initialized explicitly, then the initialization
        /// will be done at the creation of the first session (see also: RGK::Model::Session)
        /// <returns>
        /// - Result::Success in case of successful execution
        /// - Result::KernelAlreadyStarted in case when the kernel has already been started
        /// - Result::KernelStartError in case of kernel initialization failure. The problems may commonly occur due to initialization of the device for multiprocessing calculations.
        /// - Result::KernelStartPartiallySuccessful in case when a non-critical error occurs during the initialization process
        /// - Result::KernelAlreadyStartedPartiallySuccessful in case when the kernel has already been started with a non-critical error
        /// - Result::KernelAlreadyStartedError in case when the kernel had already been started, but a critical error occurred
        ///</returns>
#else
        /// Стартовать ядро.
        /// Если ядро не было инициализировано явно,
        /// то инициализация выполняется при создании первой сессии (см. RGK::Model::Session)
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelAlreadyStarted в случае, если ядро уже запущено
        /// - Result::KernelStartError в случае ошибки инициализации ядра. Обычно проблемы могут быть связаны с инициализацией устройства для массовых параллельных вычислений
        /// - Result::KernelStartPartiallySuccessful в случае возникновения некритичной ошибки в процессе инициализации
        /// - Result::KernelAlreadyStartedPartiallySuccessful в случае, если ядро уже запущено с некритичной ошибкой
        /// - Result::KernelAlreadyStartedError в случае, если ядро уже запускалось и возникла ошибка критичная для выполнения ядра
        ///</returns>
#endif
        static Common::Result Start();

#ifdef ENGLISH_HELP
        /// Stop the kernel workflow
        /// <returns>
        /// - Result::Success in case of successful execution
        /// - Result::KernelAlreadyStopped in case when the kernel has already been stopped
        /// - Result::KernelNotStarted in case when the kernel hasn't been started
        ///</returns>
#else
        /// Завершить работу ядра
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelAlreadyStopped в случае, если ядро уже было остановлено
        /// - Result::KernelNotStarted в случае, если ядро не было запущено
        ///</returns>
#endif
        static Common::Result End();

#ifdef ENGLISH_HELP
        /// Current kernel start state
#else
        /// Текущее состояние запуска ядра
#endif
        enum DLLEXPORT State
        {
#ifdef ENGLISH_HELP
            /// The kernel hasn't been started
#else
            /// Ядро не запускалось
#endif
            NotStarted,
#ifdef ENGLISH_HELP
            /// The kernel has been successfully started
#else
            /// Ядро успешно запущено
#endif
            Started,
#ifdef ENGLISH_HELP
            /// The kernel has been stopped
#else
            /// Ядро остановлено
#endif
            Stopped,
#ifdef ENGLISH_HELP
            /// The kernel has been started, non-critical errors have occurred
#else
            /// Ядро запущено с некритичными ошибками
#endif
            Partial,
#ifdef ENGLISH_HELP
            /// The kernel failed to start due to some critical errors
#else
            /// Ядро не запустилось из-за критических ошибок
#endif
            Error
        };

#ifdef ENGLISH_HELP
        /// Get the kernel state
        /// <param name="oState">Kernel state</param>
        /// <returns>
        /// - Result::Success in case of successful execution
        ///</returns>
#else
        /// Получить состояние ядра
        /// <param name="oState">Состояние ядра</param>
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        ///</returns>
#endif
        static Common::Result GetState(State &oState);

        /// Задать максимальное количество потоков, выполняемых на центральном процессоре
        /// <param name="iMaxThreads">Максимальное количество потоков, выполняемых на центральном процессоре</param>
        /// <remarks>Параметры должны задаваться до старта ядра</remarks>
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelAlreadyStarted в случае, если ядро уже запущено
        ///</returns>
        static Common::Result SetMaxThreads(int iMaxThreads);

        /// Получить максимальное количество потоков, выполняемых на центральном процессоре
        /// <param name="oMaxThreads">Максимальное количество потоков, выполняемых на центральном процессоре</param>
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelNotStarted в случае, если ядро не было запущено
        /// - Result::KernelAlreadyStopped в случае, если ядро уже было остановлено
        ///</returns>
        static Common::Result GetMaxThreads(int& oMaxThreads);

		/// Тип устройства для массовых параллельных вычислений
        enum DLLEXPORT DeviceType
        {
            /// GPU-устройство
            GPU,
            /// CPU-устройство
            CPU,
            /// Ускоритель
            Accelerator,
            /// Устройство, которое будет позволять работать с gl-данными
            GLDevice,
			/// Неизвестное (неопределённое) устройство
            UndefinedDevice
        };

		/// Задать тип устройства для массовых параллельных вычислений
        /// <param name="iDeviceType">Тип устройства для массовых параллельных вычислений</param>
        /// <remarks>Параметры должны задаваться до старта ядра</remarks>
		/// <remarks>Задание типа устройста имеет смысл только в конфигруации библиотеки, в которой поддерживается OpenCL</remarks>
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelAlreadyStarted в случае, если ядро уже запущено
        ///</returns>
        static Common::Result SetDeviceType(DeviceType iDeviceType);

        /// Получить тип устройства для массовых параллельных вычислений
        /// <param name="oDeviceType">Тип устройства для массовых параллельных вычислений</param>
		/// <remarks>Получение типа устройста имеет смысл только в конфигруации библиотеки, в которой поддерживается OpenCL</remarks>
        /// <returns>
        /// - Result::Success в случае успешного выполнения
        /// - Result::KernelNotStarted в случае, если ядро не было запущено
        /// - Result::KernelAlreadyStopped в случае, если ядро уже было остановлено
        ///</returns>
        static Common::Result GetDeviceType(DeviceType& oDeviceType);

#ifdef ENGLISH_HELP
        /// Get the string corresponding to the result code
        /// <param name="code">Result code value</param>
        /// <returns>The string corresponding to the result code</returns>
#else
        /// Получение строки, соответствующей коду результата
        /// <param name="iCode">Значение кода результата</param>
		/// <param name="iGetID">В строке возвращать код результата</param>
        /// <returns>Строка, соответствующая коду результата</returns>
#endif
        static Common::String GetResultString(Common::Result iCode,bool iGetID=false);

#ifdef ENGLISH_HELP
        /// Get the kernel version
        /// <returns>Kernel version</returns>
#else
        /// Получить номер версии ядра
        /// <returns>Номер версии</returns>
#endif
        static Common::Version GetVersion();

#ifdef ENGLISH_HELP
        /// Set the implementation of the interface of MeshBufferFactory. This is a factory for the class implementing the
        /// input buffer for mesh generator results.
        /// <param name="iFactory">Factory for the class implementing the input buffer for mesh generator results</param>
#else
        /// Установить реализацию интерфейса фабрики класса, реализующего работу с буфером, в который пишутся результаты работы сеточного генератора
        /// <param name="iFactory">Фабрика класса, реализующего работу с буфером, в который пишутся результаты работы сеточного генератора</param>
#endif
        static Common::Result SetMeshBufferFactory(Interfaces::MeshBufferFactoryPtr iFactory);

#ifdef ENGLISH_HELP
        /// Get the implementation of the interface of MeshBufferFactory. This is a factory for the class implementing the
        /// input buffer for mesh generator results.
        /// <returns>Factory for the class implementing the input buffer for mesh generator results</returns>
#else
        /// Получить реализацию интерфейса фабрики класса, реализующего работу с буфером, в который пишутся результаты работы сеточного генератора
        /// <returns>Фабрика класса, реализующего работу с буфером, в который пишутся результаты работы сеточного генератора</returns>
#endif
        static Interfaces::MeshBufferFactoryPtr GetMeshBufferFactory();

#ifdef ENGLISH_HELP
        /// Set the class for error checking
        /// <param name="iErrorChecking">Error checking class</param>
#else
        /// Установить класс обработки уведомлений об ошибках
        /// <param name="iErrorChecking">Класс обработки уведомлений об ошибках</param>
#endif
        static Common::Result SetErrorChecking(Interfaces::ErrorCheckingPtr iErrorChecking);

#ifdef ENGLISH_HELP
        /// Get the class for error checking
        /// <param name="iErrorChecking">Error checking class</param>
#else
        /// Получить класс обработки уведомлений об ошибках
        /// <returns>Класс обработки уведомлений об ошибках</returns>
#endif
        static Interfaces::ErrorCheckingPtr GetErrorChecking();

//DOM-IGNORE-BEGIN

        /// Функция обработки ошибки
        /// <param name="iResult">Значение кода ошибки</param>
        /// <returns>Ретранслированный код ошибки</returns>
        static Common::Result CheckResult(Common::Result iResult);

        /// Функция обработки ошибки
        /// <param name="iContext">Контекст вычисления</param>
        /// <param name="iError">Отчёт об ошибке</param>
        /// <returns>Ретранслированный код ошибки</returns>
        static Common::Result CheckResult(Common::Context* iContext, Common::ErrorReportPtr iError);

        /// Функция обработки ошибки
        /// <param name="iContext">Контекст вычисления</param>
        /// <param name="iResult">Значение кода ошибки</param>
        /// <returns>Ретранслированный код ошибки</returns>
        static Common::Result CheckResult(Common::Context* iContext, Common::Result iResult);

        static unsigned int GetMaxThreads();

        /// Получить текущую платформу для вычислений на OpenCL
        static const OpenCL::CLPlatform* GetCLPlatform();

        /// Проверка была ли инициализированна платформа
        static bool IsCLInitialized();
//DOM-IGNORE-END
    };
}

