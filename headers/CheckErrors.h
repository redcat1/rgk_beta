﻿// Check.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание классов ошибок для RGK::Model::Check
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include <vector>

#include "decl.h"
#include "forward.h"
#include "Common/BaseTools.h"
#include "Model/Topols/Body.h"


namespace RGK
{
    namespace Tools
    {
        namespace Validation
        {
#ifdef ENGLISH_HELP
            /// <summary> Class of an error was found during the model check</summary>
            /// Class of an error
			/// <seealso cref="Model validation"/>
#else
            /// <summary>Класс ошибки, выявленной при проверке модели</summary>
            /// Класс ошибки
            /// <seealso cref="Проверка модели"/>
#endif
            class DLLEXPORT CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Type of an error
#else
                /// Тип ошибки
#endif
                enum ErrorType
                {
#ifdef ENGLISH_HELP
                    /// Orientation of an edge and a coedge is set inconsistent relatively a curve orientation
#else
                    /// Ориентация ребра и R-ребра установлена неверно относительно ориентации кривой
#endif
                    InconsistentOrientationWithEdge = 0,
#ifdef ENGLISH_HELP
					/// A wire body is connected but edges are oriented inconsistent
#else
                    /// Проволочное тело связно, но ребра ориентированы несовместимо
#endif
                    InconsistentOrientationOfWireBody,
#ifdef ENGLISH_HELP
					/// Coedge has got an incorrect GetNext pointer
#else
                    /// Некорректный GetNext указатель у R-ребра
#endif
                    IncorrectGetNextPointer,
#ifdef ENGLISH_HELP
					/// Coedge has got an incorrect GetNext pointer
#else
                    /// Некорректный GetPrev указатель у R-ребра
#endif
                    IncorrectGetPrevPointer,
#ifdef ENGLISH_HELP
					/// Coedge has got a null-pointer to an edge
#else
                    /// У R-ребра указатель на ребро равен нулю
#endif
                    EdgeIsNullptr,
#ifdef ENGLISH_HELP
					/// Incorrect type of a loop
#else
                    /// Некорректный тип цикла
#endif
                    IncorrectLoopType,
#ifdef ENGLISH_HELP
					/// Pointer to a coedge is null
#else
                    /// Указатель на R-ребро равен нулю
#endif
                    HasNullCoEdges,
#ifdef ENGLISH_HELP
					/// Two coedges are not connected
#else
                    /// Два R-ребра не связанны
#endif
                    NotConnected,
#ifdef ENGLISH_HELP
					/// Incorrect relationship between loops
#else
                    /// Неправильное отношение между циклами
#endif
                    LoopsHasIncorrectRelationship,
#ifdef ENGLISH_HELP
					/// Loop contains another one completely
#else
                    /// Цикл содержит полностью другой цикл
#endif
                    LoopContainsAnotherLoop,
#ifdef ENGLISH_HELP
					/// Loop has got selfintersections
#else
                    /// Цикл имеет самопересечения
#endif
                    LoopHasSelfIntersection,
#ifdef ENGLISH_HELP
					/// Loops have not got a common area
#else
                    /// Циклы не имеют общей площади
#endif
                    LoopsHasNoCommonArea,
#ifdef ENGLISH_HELP
					/// A loop is not connected
#else
                    /// Цикл не связен
#endif
                    LoopNotConnected,
#ifdef ENGLISH_HELP
					/// Loops on an edge have got a common vertex
#else
					/// Циклы на грани имеют общую вершину
#endif
					LoopsHaveCommonVertices,
#ifdef ENGLISH_HELP
					/// Loop has got an undefined type
#else
                    /// Цикл имеет неопределенный тип
#endif
                    LoopTypeIsUndefined,
#ifdef ENGLISH_HELP
					/// Face is not bounded
#else
                    /// Грань не ограничена
#endif
                    FaceNotBounded,
#ifdef ENGLISH_HELP
					/// Face is not connected
#else
                    /// Грань не связна
#endif
                    FaceNotConnected,
#ifdef ENGLISH_HELP
					/// Normals of faces are incosistent
#else
                    /// Нормали граней не согласованы
#endif
                    FaceNormalsAreNotConsistent,
#ifdef ENGLISH_HELP
					/// Face has not got a surface
#else
                    /// Нет поверхности привязанной к грани
#endif
                    FaceHasNoSurface,
#ifdef ENGLISH_HELP
					/// Edges have got an intersection
#else
                    /// Ошибка: ребра пересекаются
#endif
                    EdgeIntersection,
#ifdef ENGLISH_HELP
					/// Faces have got an intersection
#else
                    /// Ошибка: грани пересекаются
#endif
                    FaceIntersection,
#ifdef ENGLISH_HELP
					/// Vertex of an edge is inconsistent by an interval on a curve
#else
                    /// Вершина ребра не соответствует интервалу ребра на кривой
#endif
                    IncorrectEdgeVertex,
#ifdef ENGLISH_HELP
					/// A geometry of a coedge is not compliant with a geometry of an edge 
#else
                    /// Геометрия R-ребра не соответствует геометрии ребра.
#endif
                    EdgeCoEdgeDiscrepancy,
#ifdef ENGLISH_HELP
					/// A geometry of an edge is not compliant with a geometry of a face
#else
                    /// Геометрия ребра не соответствует геометрии грани.
#endif
                    EdgeFaceDiscrepancy,
#ifdef ENGLISH_HELP
					/// Orientation of a coedge curve is inconsistent by an orientation of an edge one
#else
                    /// Ориентация кривой R-ребра не соответствует ориентации кривой ребра
#endif
                    EdgeCoEdgeCurvesOrientation,
#ifdef ENGLISH_HELP
					/// Edge has got only single coedge in a solid body
#else
                    /// Ребро имеет только одно R-ребро в твёрдом теле
#endif
                    EdgeHasOneCoEdge,
#ifdef ENGLISH_HELP
					/// There is an edge with count of coedges other than 2
#else
                    /// Если есть ребро не с 2 R-рёбрами
#endif
                    ShellContainEdgeWithIncorrectCoedgesNumber,
#ifdef ENGLISH_HELP
                    /// There are more than one components of connectivity of faces
#else
                    /// Если больше одной компоненты связанности граней
#endif
                    ShellContainSeveralAdjacencyComponents,
#ifdef ENGLISH_HELP
					/// A discrepancy of a geometry of a coedges pair is found
#else
                    /// Геометрия пары R-рёбер не соответствует 
#endif
                    CoEdgeNeighbourDiscrepancy,
#ifdef ENGLISH_HELP
					/// Coedge has not got a loop
#else
                    /// Если в R-грани нет цикла
#endif
                    ShellCoedgeHasNoLoop,
#ifdef ENGLISH_HELP
					/// A pointer to a parent element is incorrect
#else
                    // Указатель на родительский элемент неверен
#endif
                    IncorrectParentPtr,
#ifdef ENGLISH_HELP
					/// A pointer to a body is incorrect
#else
                    /// Указатель на тело некорректен
#endif
                    IncorrectBodyPtr,
#ifdef ENGLISH_HELP
					/// The body has not got a pointer to this element
#else
                    /// В Body нет указателя на этот элемент
#endif
                    TopolNotFoundInBody,
#ifdef ENGLISH_HELP
					/// The topological item has been added to a body but is used nowhere
#else
                    /// Элемент добавлен в тело, но нигде не используется
#endif
                    UnusedTopol,
#ifdef ENGLISH_HELP
					/// A pointer to an adjacent item is incorrect: a coedge from another shell is marked as an adjacent coedge
#else
                    /// Указатель на смежный элемент неверен: в качестве смежного R-ребра помечено ребро находящееся в другой оболочке
#endif
                    IncorrectNeighbour,
#ifdef ENGLISH_HELP
					/// A face of a solid body has to have got no more than one coface in one shell
#else
                    /// У грани Solid тела количество R-граней грани в одной оболочке не должно превышать 1
#endif
                    IncorrectCoFacesNumberInSolid,
#ifdef ENGLISH_HELP
					/// Face has not got any cofaces 
#else
                    /// У грани отсутствуют R-грани
#endif
                    FaceHasNoCoFaces,
#ifdef ENGLISH_HELP
					/// Any face
#else
                    /// У каждой грани в не замкнутом листовом теле должно быть 2 R-грани
#endif
                    IncorrectCoFacesNumberInSheet,
#ifdef ENGLISH_HELP
					/// Any face in a closed sheet body has to have got one coface
#else
                    /// У каждой грани в замкнутом листовом теле должна быть 1 R-грань
#endif
                    IncorrectCoFacesNumberInClosedSheet,
#ifdef ENGLISH_HELP
					/// The orientation of a codace in a closed sheet body does not coincide with an orientation of a face
#else
                    /// Ориентация R-грани у замкнутого листового тела не совпадает с ориентацией грани 
#endif
                    IncorrectCoFaceOrientationInClosedSheet,
#ifdef ENGLISH_HELP
					/// Two cofaces of a single face in a sheet body have got the same orientation relatively the face
#else
                    /// Ошибка возникает, если две R-грани одной грани в листовом теле имеют одинаковую ориентацию относительно грани
#endif
                    CoFaceOrientationAreEqual,
#ifdef ENGLISH_HELP
					/// An orientation of a coface in a solid body does not coincide with an orientation of a face
#else
                    /// Ориентация R-грани у твёрдого тела не совпадает с ориентацией грани 
#endif
                    IncorrectCoFaceOrientation,
#ifdef ENGLISH_HELP
					/// An unexpected face in a wire body has been found
#else
                    /// У проволочного тела обнаружена грань
#endif
                    UnexpectedFace,
#ifdef ENGLISH_HELP
					/// A curve of a coedge does not lie on a surface of a face
#else
                    /// Кривая R-ребра расположена не на поверхности грани.
#endif
                    IncorrectCoEdgeCurveSurface,
#ifdef ENGLISH_HELP
					/// An unexpected region has been found in a sheetbody or wire body
#else
                    /// Ошибка для случая, если у листового или проволочного тела есть регион
#endif
                    UnexpectedRegion,
#ifdef ENGLISH_HELP
					/// A solid body has not to have got any regions for infinite volumes
#else
                    /// Твёрдое тело не должно содержать регионов для бесконечных объёмов
#endif
                    UnexpectedVoidRegion,
#ifdef ENGLISH_HELP
                    /// Region has to bound a finit area
#else
                    /// Регион должен ограничивать конечную область
#endif
                    RegionNotBounded,
#ifdef ENGLISH_HELP
					/// A region of a solid body has to have got one shell at least
#else
                    /// Регион твёрдого тела, должен содержать как минимум одну оболочку
#endif
                    IncorrectShellNumber,
#ifdef ENGLISH_HELP
                    /// A wire body has got a shell
#else
                    /// Ошибка для случая, если у проволочного тела есть оболочка
#endif
                    UnexpectedShell,
#ifdef ENGLISH_HELP
                    /// A wire body has got more than one connectivity component
#else
                    /// Проволочное тело содержит больше одной компоненты связности
#endif
                    IncorrectNumberOfComponents,
#ifdef ENGLISH_HELP
                    /// A shell contains a face that has not got any pointers to this shell
#else
                    /// Оболочка содержит грань, у которой нет указателей на эту оболочку
#endif
                    ShellContainsForeignFace,
#ifdef ENGLISH_HELP
                    /// A shell contains null-references to elements
#else
                    /// Оболочка содержит нулевые ссылки на элементы
#endif
                    ShellHasNullElements,
#ifdef ENGLISH_HELP
					/// A face is included to a shell some times
#else
                    /// Face входит в Shell несколько раз
#endif
                    DuplicateFaceInShell,
#ifdef ENGLISH_HELP
					/// A region has not got a single shell
#else
                    /// Нет единственной внешней оболочки в регионе
#endif
                    IncorrectOuterShell,
#ifdef ENGLISH_HELP
					/// A region contains a shell with an incorrect orientation
#else
                    /// Регион содержит некорректно ориентированную оболочку
#endif
                    IncorrectShellsRelation,
#ifdef ENGLISH_HELP
					/// A vertex has not got any geometry
#else
                    /// У вершины отсутствует геометрия
#endif
                    VertexHasNoPoint,
#ifdef ENGLISH_HELP
					/// A vertex is out of a size of a model
#else
                    /// Вершина выходит за габариты моделирования
#endif
                    VertexOutOfModelSize,
#ifdef ENGLISH_HELP
                    /// An edge is out of a size of a model
#else
                    /// Ребро выходит за габариты моделирования
#endif
                    EdgeOutOfModelSize,
#ifdef ENGLISH_HELP
					/// A face is out of a size of a model
#else
                    /// Грань выходит за габариты моделирования
#endif
                    FaceOutOfModelSize,
#ifdef ENGLISH_HELP
					/// An edge contains a null-pointer to a vertex
#else
                    /// Ребро содержит нулевой указатель на вершину
#endif
                    EdgeHasNullVertex,
#ifdef ENGLISH_HELP
					/// An interval of an edge has got a zero-length
#else
                    /// Интервал ребра имеет нулевую длину
#endif
                    EdgeHasZeroLengthInterval,
#ifdef ENGLISH_HELP
                    /// An interval of an edge has got a negative length
#else
                    /// Интервал ребра имеет отрицательную длину
#endif
                    EdgeHasNegativeLengthInterval,
#ifdef ENGLISH_HELP
                    /// An edge has a zero length
#else
                    /// Длина ребра равна нулю
#endif
                    EdgeHasZeroLengthCurve,
#ifdef ENGLISH_HELP
                    /// An interval of an edge has not got finite type
#else
                    /// Тип интервала ребра не конечный
#endif
                    EdgeHasNotFiniteInterval,
#ifdef ENGLISH_HELP
					/// An edge interval is not nested in a interval of a curve
#else
                    /// Интервал ребра не вложен в интервал кривой
#endif
                    EdgeIntervalIsNotNestedInCurveInterval,
#ifdef ENGLISH_HELP
					/// An interval of an edge is more then an interval of a curve
#else
                    /// Интервал ребра больше интервала кривой
#endif
                    EdgeIntervalIsGreaterThanCurveInterval,
#ifdef ENGLISH_HELP
                    /// A vertex of an edge does not lie on the corresponded end of a curve
#else
                    /// Вершина ребра не лежит на соответствующем конце кривой
#endif
                    EdgeHasVertexInIncorrectPlace,
#ifdef ENGLISH_HELP
					An edge or coedge has not got an associated curve
#else
                    // Нет кривой привязанной к ребру или R-ребру
#endif
                    EdgeHasNoAssignedCurve,
#ifdef ENGLISH_HELP
					/// An edge has got more than two coedges
#else
                    /// Ребро имеет больше двух R-рёбер
#endif

                    EdgeIsNotManifold,
#ifdef ENGLISH_HELP
					/// A coedge has got a null pointer to an edge
#else
                    /// У R-ребра указатель на ребро равен нулю
#endif
                    CoEdgeHasNullEdge,
#ifdef ENGLISH_HELP
					/// A coedge has got an incorrect pointer to a face
#else
                    /// У R-ребра не корректный указатель на грань
#endif
                    CoEdgeHasIncorrectFacePtr,
#ifdef ENGLISH_HELP
					/// A coedge and edge has not got any curves
#else
                    /// У R-ребра и ребра нет кривой 
#endif
                    CoEdgeHasNoAssignedCurve,
#ifdef ENGLISH_HELP
                    /// A coedge interval is not nested in a curve interval
#else
                    /// Интервал R-ребра не вложен в интервал кривой
#endif
                    CoEdgeIntervalIsNotNestedInCurveInterval,
#ifdef ENGLISH_HELP
                    /// A coedge interval is greater then a curve one
#else
                    /// Интервал R-ребра больше интервала кривой
#endif
                    CoEdgeIntervalIsGreaterThanCurveInterval,
#ifdef ENGLISH_HELP
                    /// Interval of a coedge has a zero-length
#else
                    // Интервал R-ребра имеет нулевую длину
#endif
                    CoEdgeHasZeroLengthInterval,
#ifdef ENGLISH_HELP
					/// A length of a coedge is equal zero
#else
                    // Длина R-ребра равна нулю
#endif
                    CoEdgeHasZeroLengthCurve,

#ifdef ENGLISH_HELP
					/// A coedge has got a negative length interval
#else
                    /// Длина R-ребра отрицательна
#endif
                    CoEdgeHasNegativeLengthInterval,
#ifdef ENGLISH_HELP
					/// A coedge has not got an interval with a finite type
#else
                    /// Тип интервала R-ребра не конечный
#endif
                    CoEdgeHasNotFiniteInterval,
#ifdef ENGLISH_HELP
					/// Type of a coedge interval is not coincide with a type of an edge one
#else
					/// Тип интервала R-ребра не совпадает с интервалом ребра
#endif
					CoEdgeIntervalIsNotEqualToEdgeInterval,
#ifdef ENGLISH_HELP
					/// A coedge is out of model size
#else
                    /// R-Ребро выходит за габариты моделирования
#endif
                    CoEdgeOutOfModelSize,
#ifdef ENGLISH_HELP
					/// Vertices of a coedge do not coincide with correspondinf ends of coedge curve
#else
                    /// Вершины ребра не совпадают с соответствующими концами кривой R-ребра
#endif
                    CoEdgeHasVertexInIncorrectPlace,
#ifdef ENGLISH_HELP
					/// Coedge has to lie on a boundary of a periodic surface
#else
                    /// R-ребро должно лежать на границе периодической поверхности
#endif
					CoEdgeIsNotOnPeriodicSurfaceBoundary,
#ifdef ENGLISH_HELP
					/// An orientation of a coedge is incorrect
#else
                    /// Ориентация R-ребра не корректна
#endif
                    CoEdgeHasIncorrectOrientation,
#ifdef ENGLISH_HELP
					/// A face has not got an outer loop
#else
                    // У грани отсутствует внешний цикл
#endif
                    FaceHasNoOuterLoop,
#ifdef ENGLISH_HELP
					/// A face has got more than one outer loops
#else
                    /// У грани больше одного внешнего цикла
#endif
                    FaceHasMoreThanOneOuterLoop,
#ifdef ENGLISH_HELP
					/// A face has not any loops
#else
                    /// У грани нет циклов
#endif
                    FaceHasNoLoops,
#ifdef ENGLISH_HELP
					/// A vertex loop has got coedges
#else
                    /// В вершинном цикле присутствуют R-ребра
#endif
                    VertexLoopHasCoEdges,
#ifdef ENGLISH_HELP
					/// Loop has not got any coedges
#else
                    /// У цикла отсутствуют R-рёбра
#endif
                    LoopHasNoCoEdges,
#ifdef ENGLISH_HELP
					/// A loop is not wire
#else
                    /// Цикл не является проволочным
#endif
                    LoopIsNotWire,
#ifdef ENGLISH_HELP
					/// An edge has got less or more then 2 occurrences to the wire loop
#else
                    /// Ребро входит не 2 раза в проволочный цикл
#endif
                    EdgeHasIncorrectNumberOfOccurrences,
#ifdef ENGLISH_HELP
					/// An edge has a incorrect pointer to an coedge
#else
                    /// Ребро имеет некорректный указатель на R-ребро
#endif
                    EdgeHasIncorrectCoEdgePointer,
#ifdef ENGLISH_HELP
					/// A region has got more than one outer shells
#else
                    /// Регион имеет больше одной внешней оболочке
#endif
                    RegionHasMoreThanOneOuterShell,
#ifdef ENGLISH_HELP
					/// There are no any outer shells in a region
#else
                    /// Нет внешних оболочек в регионе
#endif
                    NoOuterShellInRegion,
#ifdef ENGLISH_HELP
					/// An incorrect nesting relationship between shells has been found
#else
                    /// Некорректное отношение вложенности между оболочками
#endif
                    IncorrectShellsRelationship,
#ifdef ENGLISH_HELP
					/// The first shell contains a volume 
#else
                    /// Первая оболочка содержит в себе объем второй оболочки
#endif
                    ShellContainsAnotherShell,
#ifdef ENGLISH_HELP
					/// Shells have not got a common volume
#else
                    /// Оболочки не имеют общего объёма
#endif
                    ShellHaveNotCommonVolume,
#ifdef ENGLISH_HELP
					/// A projection of a loop to a face is unavailable
#else
                    /// Невозможно спроецировать цикл на грань
#endif
                    LoopProjectionUnavailable,
#ifdef ENGLISH_HELP
					/// A wire body is not manifold
#else
                    /// Проволочное тело 
#endif
                    WireIsNonManifold,
#ifdef ENGLISH_HELP
					/// A body with a type "Empty" has not to consist topological elements
#else
                    /// Тело с типом Empty не должно содержать топологические элементы
#endif
                    BodyIsNotEmpty
                };

#ifdef ENGLISH_HELP
				/// Get a type of an error
				/// <returns>Type of an error</returns>
#else
                /// Получить тип ошибки
                /// <returns>Тип ошибки</returns>
#endif
                virtual ErrorType GetType() const;

#ifdef ENGLISH_HELP
				/// Get a string describes an error has been found by a model check
                /// <returns>String of an error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const;

#ifdef ENGLISH_HELP
                /// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const;

#ifdef ENGLISH_HELP
				/// Destructor
#else
                /// Деструктор
#endif
                virtual ~CheckError() {}

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iType">тип ошибки</param>
                CheckError(ErrorType iType);

                /// Создаёт ошибку и возвращает на неё ссылку
                /// <param name="iType">тип ошибки</param>
                /// <returns>ссылка на ошибку</returns>
                static CheckErrorPtr Create(ErrorType iType);

            
            private:
                ErrorType _error;
    //DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
            /// <summary>Class of an error is concerned with a coedge</summary>
            /// Class of an error is concerned with a coedge. Keeps a reference to a coedge with an error.
#else
            /// <summary>Класс ошибки, связанной с R-ребром</summary>
            /// Класс ошибки, связанной с R-ребром. Хранит ссылку на R-ребро, на котором возникла ошибка.
#endif
            class DLLEXPORT CoEdgeError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get coedge with an error
                /// <returns>Coedge</returns>
#else
                /// Получить R-ребро, на котором возникает ошибка
                /// <returns>R-ребро</returns>
#endif
                Model::CoEdgePtr GetCoEdge() const;

#ifdef ENGLISH_HELP
                /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;
    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iCoEdge"></param>
                /// <returns></returns>
                CoEdgeError(ErrorType iError, Model::CoEdgePtr iCoEdge);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iCoEdge">R-ребро</param>
                /// <returns>ссылка на ошибку</returns>
                static CoEdgeErrorPtr Create(ErrorType iError, Model::CoEdgePtr iCoEdge); 
            private:
                Model::CoEdgePtr _coedge;
    //DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with a loop</summary>
            /// Class of an error is concerned with a loop. Keeps a reference to a loop with an error.
#else
            /// <summary>Класс ошибки, связанной с циклом</summary>
            /// Класс ошибки, связанной с циклом. Хранит ссылку на цикл.
#endif
            class DLLEXPORT LoopError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
                /// Get a pointer to the loop with an error
                /// <returns>Loop</returns>
#else
                /// Получить указатель на цикл, на котором возникла ошибка
                /// <returns>Цикл, на котором возникла ошибка</returns>
#endif
                Model::LoopPtr GetLoop() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">тип ошибки</param>
                /// <param name="iLoop">цикл</param>
                LoopError(ErrorType iError, Model::LoopPtr iLoop);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iCoEdge">цикл</param>
                /// <returns>ссылка на ошибку</returns>
                static LoopErrorPtr Create(ErrorType iError, Model::LoopPtr iLoop); 
            private:
                Model::LoopPtr _loop;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with a connectivity of coedges by a loop</summary>
            /// Class of an error is concerned with a connectivity of coedges by a loop. Keeps a reference to two unconnected coedges.
#else
            /// <summary>Класс ошибки, связанные со связностью R-рёбер циклом</summary>
            /// Класс ошибки, связанной со связностью R-рёбер циклом. Хранит ссылки на два смежных R-ребра, которые не связанны.
#endif
            class DLLEXPORT CoEdgesCompatibilityError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get the first coedge
				/// <returns>Coedge</returns>
#else
                /// Получить первое R-ребро
                /// <returns>первое R-ребро</returns>
#endif
                Model::CoEdgePtr GetFirstCoEdge() const;

#ifdef ENGLISH_HELP
                /// Get the second coedge
				/// <returns>Coedge</returns>
#else
                /// Получить второе R-ребро
                /// <returns>второе R-ребро</returns>
#endif
                Model::CoEdgePtr GetSecondCoEdge() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
                /// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public: 
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iCoEdge1">первое R-ребро</param>
                /// <param name="iCoEdge2">второе R-ребро</param>
                /// <returns></returns>
                CoEdgesCompatibilityError(ErrorType iError, Model::CoEdgePtr iCoEdge1, Model::CoEdgePtr iCoEdge2);

                /// Создаёт ошибку и возвращает на неё ссылку
                /// <param name="iError">Тип Ошибки</param>
                /// <param name="iCoEdge1">первое R-ребро</param>
                /// <param name="iCoEdge2">второе R-ребро</param>
                /// <returns></returns>
                static CoEdgesCompatibilityErrorPtr Create(ErrorType iError, Model::CoEdgePtr iCoEdge1, Model::CoEdgePtr iCoEdge2);

            private:
                Model::CoEdgePtr _coedge1;
                Model::CoEdgePtr _coedge2;
    //DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with loops</summary>
            /// Class of an error is concerned with loop. Keeps a reference to two loops.
#else
            /// <summary>Класс ошибки, связанной с циклами</summary>
            /// Класс ошибки, связанной с циклом. Хранит ссылку на два цикла.
#endif
            class DLLEXPORT LoopsCompatibilityError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
                /// Get a pointer to the first loop with an error
                /// <returns>Loop</returns>
#else
                /// Получить указатель на первый цикл, на котором возникла ошибка
                /// <returns>Цикл, на котором возникла ошибка</returns>
#endif
                Model::LoopPtr GetFirstLoop() const;

#ifdef ENGLISH_HELP
				/// Get a pointer to the second loop with an error
				/// <returns>Loop</returns>
#else
                /// Получить указатель на второй цикл, на котором возникла ошибка
                /// <returns>Цикл, на котором возникла ошибка</returns>
#endif
                Model::LoopPtr GetSecondLoop() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iLoop1">Первый цикл</param>
                /// <param name="iLoop2">Второй цикл</param>
                LoopsCompatibilityError(ErrorType iError, Model::LoopPtr iLoop1, Model::LoopPtr iLoop2);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iLoop1">Первый цикл</param>
                /// <param name="iLoop2">Второй цикл</param>
                /// <returns>ссылка на ошибку</returns>
                static LoopsCompatibilityErrorPtr Create(ErrorType iError, Model::LoopPtr iLoop1, Model::LoopPtr iLoop2); 
            private:
                Model::LoopPtr _loop1;
                Model::LoopPtr _loop2;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with a face</summary>
            /// Class of an error is concerned with a face. Keeps a reference to a face with an error.
#else
            /// <summary>Класс ошибки, связанной с гранью</summary>
            /// Класс ошибки, связанной с гранью. Хранит ссылку на грань.
#endif
            class DLLEXPORT FaceError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the face with an error
                /// <returns>Face</returns>
#else
                /// Получить указатель на грань, на которой возникла ошибка
                /// <returns>Грань, на котором возникла ошибка</returns>
#endif
                Model::FacePtr GetFace() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFace">Грань</param>
                FaceError(ErrorType iError, Model::FacePtr iFace);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFace">грань</param>
                /// <returns>ссылка на ошибку</returns>
                static FaceErrorPtr Create(ErrorType iError, Model::FacePtr iFace); 
            private:
                Model::FacePtr _face;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
            /// <summary>Face error description class</summary>
#else
            /// <summary>Класс ошибки, связанной с гранью</summary>
            /// Класс ошибки, связанной с гранью. Хранит ссылку на грань.
#endif
            class DLLEXPORT RegionError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the face with an error
                /// <returns>Face</returns>
#else
                /// Получить указатель на грань, на которой возникла ошибка
                /// <returns>Грань, на котором возникла ошибка</returns>
#endif
                Model::RegionPtr GetRegion() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iRegion">Регион</param>
                RegionError(ErrorType iError, Model::RegionPtr iRegion);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iRegion">Регион</param>
                /// <returns>ссылка на ошибку</returns>
                static RegionErrorPtr Create(ErrorType iError, Model::RegionPtr iRegion); 
            private:
                Model::RegionPtr _region;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with an intersection of body elements</summary>
            /// Class of an error is with an intersection of body elements. Keeps a reference to a pair of crossing topological items.
#else
            /// <summary>Класс ошибки, связанной с пересечением элементов тела</summary>
            /// Класс ошибки, связанной с пересечением элементов тела. Хранит ссылку на пару Topol которые пересекаются.
#endif
            class DLLEXPORT IntersectionError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the first topological item with an error
                /// <returns>Topological item</returns>
#else
                /// Получить указатель на первый Topol, на котором возникает ошибка
                /// <returns>Элемент на котором произошла ошибка</returns>
#endif
                Model::TopolPtr GetFirst() const;

#ifdef ENGLISH_HELP
				/// Get a pointer to the second topological item with an error
                /// <returns>Topological item</returns>
#else
                /// Получить указатель на второй Topol, на котором возникает ошибка
                /// <returns>Элемент на котором произошла ошибка</returns>
#endif
                Model::TopolPtr GetSecond() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol1">Первый элемент</param>
                /// <param name="iTopol2">Второй элемент</param>
                /// <returns></returns>
                IntersectionError(ErrorType iError, Model::TopolPtr iTopol1, Model::TopolPtr iTopol2);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol1">Первый элемент</param>
                /// <param name="iTopol2">Второй элемент</param>
                /// <returns>ссылка на ошибку</returns>
                static IntersectionErrorPtr Create(ErrorType iError, Model::TopolPtr iTopol1, Model::TopolPtr iTopol2); 
            private:
                Model::TopolPtr _first;
                Model::TopolPtr _second;
    //DOM-IGNORE-END
            };


#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with faces</summary>
            /// Class of an error is concerned with face. Keeps a reference to two faces.
#else
            /// <summary>Класс ошибки, связанной с гранями</summary>
            /// Класс ошибки, связанной с гранями. Хранит ссылки на две грани.
#endif
            class DLLEXPORT FacesCompatibilityError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
                /// Get a pointer to the first face with the error
                /// <returns>First face</returns>
#else
                /// Получить указатель на первую грань, на которой возникла ошибка
                /// <returns>Первая грань, на которой возникла ошибка</returns>
#endif
                Model::FacePtr GetFirstFace() const;

#ifdef ENGLISH_HELP
                /// Get a pointer to the second face with the error
                /// <returns>Second face</returns>
#else
			    /// Получить указатель на вторую грань, на которой возникла ошибка
                /// <returns>Вторая грань, на которой возникла ошибка</returns>
#endif
                Model::FacePtr GetSecondFace() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFace1">Первая грань</param>
                /// <param name="iFace2">Вторая грань</param>
                FacesCompatibilityError(ErrorType iError, Model::FacePtr iFace1, Model::FacePtr iFace2);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFace1">Первая грань</param>
                /// <param name="iFace2">Вторая грань</param>
                /// <returns>Ссылка на ошибку</returns>
                static FacesCompatibilityErrorPtr Create(ErrorType iError, Model::FacePtr iFace1, Model::FacePtr iFace2); 
            private:
                Model::FacePtr _face1;
                Model::FacePtr _face2;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with vertices of an edge</summary>
            /// Class of an error is cincerned vertices of an edge. Keeps a reference to a vertex and an edge.
#else
            /// <summary>Класс ошибки, связанной с вершинами ребра</summary>
            /// Класс ошибки, связанной с вершинами ребра. Хранит ссылку на вершину и ребро.
#endif
            class DLLEXPORT EdgeVertexError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
                /// Get a pointer to the edge the vertex belong to
                /// <returns>Edge</returns>
#else
                /// Получить указатель на ребро, которому принадлежит вершина
                /// <returns>Ребро, которому принадлежит вершина</returns>
#endif
                Model::EdgePtr GetEdge() const;

#ifdef ENGLISH_HELP
                /// Get a pointer to the vertex with the error
                /// <returns>Vertex</returns>
#else
                /// Получить указатель на вершину, на которой возникла ошибка
                /// <returns>Вершина, на которой произошла ошибка</returns>
#endif
                Model::VertexPtr GetVertex() const;

#ifdef ENGLISH_HELP
                /// Get error value
#else
                /// Получить величину ошибки
                /// <returns>Величина ошибки</returns>
#endif
                double GetError() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iEdge">Ребро на котором найдена вершина</param>
                /// <param name="iVertex">Вершина на которой обнаружена ошибка</param>
                /// <param name="iErrorValue">Величина ошибки</param>
                EdgeVertexError(ErrorType iError, Model::EdgePtr iEdge, Model::VertexPtr iVertex, double iErrorValue);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iEdge">Ребро на котором найдена вершина</param>
                /// <param name="iVertex">Вершина на которой обнаружена ошибка</param>
                /// <param name="iErrorValue">Величина ошибки</param>
                /// <returns>Ссылка на ошибку</returns>
                static EdgeVertexErrorPtr Create(ErrorType iError, Model::EdgePtr iEdge, Model::VertexPtr iVertex, double iErrorValue); 
            private:
                Model::EdgePtr _edge;
                Model::VertexPtr _vertex;
                double  _error;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with an incosistence of a geometry of connected topological elements</summary>
#else
            /// <summary>Класс ошибки, связанной с несоответствием геометрии связанных топологических элементов</summary>
#endif
            class DLLEXPORT DiscrepancyError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
		        /// Get a pointer to the first item with the error
                /// <returns>Topological item</returns>
#else
                /// Получить указатель на первый элемент, на котором возникла ошибка
                /// <returns>Элемент, на котором возникла ошибка</returns>
#endif
                Model::TopolPtr GetFirst() const;

#ifdef ENGLISH_HELP
				/// Get a pointer to the second item with the error
                /// <returns>Topological item</returns>
#else
                /// Получить указатель на второй элемент, на котором возникла ошибка
                /// <returns>Элемент, на котором возникла ошибка</returns>
#endif
                Model::TopolPtr GetSecond() const;

#ifdef ENGLISH_HELP
                /// Get a value of the error
                /// <returns>Value of the error</returns>
#else
                /// Получить величину ошибки
                /// <returns>Величина ошибки</returns>
#endif
                double GetError() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFirst">Первый элемент</param>
                /// <param name="iSecond">Второй элемент</param>
                /// <param name="iErrorValue">Величина ошибки</param>
                DiscrepancyError(ErrorType iError, Model::TopolPtr iFirst, Model::TopolPtr iSecond, double iErrorValue);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iFirst">Первый элемент</param>
                /// <param name="iSecond">Второй элемент</param>
                /// <param name="iErrorValue">Величина ошибки</param>
                /// <returns>Ссылка на ошибку</returns>
                static DiscrepancyErrorPtr Create(ErrorType iError, Model::TopolPtr iFirst, Model::TopolPtr iSecond, double iErrorValue);
            private:
                Model::TopolPtr _first;
                Model::TopolPtr _second;
                double  _error;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with a shell</summary>
            /// Class of an error is concerned with a shell. Keeps a reference to the shell.
#else
            /// <summary>Класс ошибки, связанной с оболочкой</summary>
            /// Класс ошибки, связанной с оболочкой. Хранит ссылку на оболочку.
#endif
            class DLLEXPORT ShellError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the shell with the error
                /// <returns>Shell</returns>
#else
                /// Получить указатель на оболочку, на которой возникла ошибка
                /// <returns>Оболочка, на котором возникла ошибка</returns>
#endif
                Model::ShellPtr GetShell() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iShell">Оболочка</param>
                ShellError(ErrorType iError, Model::ShellPtr iShell);
    

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iShell">грань</param>
                /// <returns>ссылка на ошибку</returns>
                static ShellErrorPtr Create(ErrorType iError, Model::ShellPtr iShell); 
            private:
                Model::ShellPtr _shell;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with an inconsistence of edge parameters and coedge parameters</summary>
#else
            /// <summary>Класс ошибки, связанной с несоответствием параметров ребра и R-ребра</summary>
#endif
            class DLLEXPORT EdgeCoEdgeCompatibilityError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the edge with the error
                /// <returns>Edge</returns>
#else
                /// Получить указатель на ребро, на котором возникла ошибка
                /// <returns>Ребро, на котором возникла ошибка</returns>
#endif
                Model::EdgePtr GetEdge() const;

#ifdef ENGLISH_HELP
				/// Get a pointer to the coedge with the error
                /// <returns>Coedge</returns>
#else
                /// Получить указатель на R-ребро, на котором возникла ошибка
                /// <returns>R-ребро, на котором возникла ошибка</returns>
#endif
                Model::CoEdgePtr GetCoEdge() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iEdge">Ребро, на котором возникла ошибка</param>
                /// <param name="iCoEdge">R-ребро, на котором возникла ошибка</param>
                EdgeCoEdgeCompatibilityError(ErrorType iError, Model::EdgePtr iEdge, Model::CoEdgePtr iCoEdge);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iEdge">Ребро, на котором возникла ошибка</param>
                /// <param name="iCoEdge">R-ребро, на котором возникла ошибка</param>
                /// <returns>Ссылка на ошибку</returns>
                static EdgeCoEdgeCompatibilityErrorPtr Create(ErrorType iError, Model::EdgePtr iEdge, Model::CoEdgePtr iCoEdge);
            private:
                Model::EdgePtr _edge;
                Model::CoEdgePtr _coEdge;
    //DOM-IGNORE-END            
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with pointers in topological items</summary>
            /// Class of an error is concerned with pointers in topological items. Keeps a reference to a topological item with the error.
#else
            /// <summary>Класс ошибки, связанной с указателями в топологических элементах</summary>
            /// Класс ошибки, связанной с указателями в топологических элементах. Хранит ссылку на топологический элемент, на котором возникла ошибка.
#endif
            class DLLEXPORT TopolPtrError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the topological item with the error
                /// <returns>Topological item</returns>
#else
                /// Получить топологический элемент, на котором возникает ошибка
                /// <returns>Указатель на топологический элемент</returns>
#endif
                Model::TopolPtr GetTopol() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

    //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol">Элемент на котором произошла ошибка</param>
                /// <returns></returns>
                TopolPtrError(ErrorType iError, Model::TopolPtr iTopol);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol">Элемент на котором произошла ошибка</param>
                /// <returns>ссылка на ошибку</returns>
                static TopolPtrErrorPtr Create(ErrorType iError, Model::TopolPtr iTopol);
            private:
                Model::TopolPtr _topol;
    //DOM-IGNORE-END
            };


#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with one topological item</summary>
            /// Class of an error is concerned with one topological items. Keeps a reference to a topological item with the error.
#else
            /// <summary>Класс ошибки, связанный с одним топологическим элементом</summary>
            /// Класс ошибки, связанный с одним топологическим элементом. Хранит ссылку на топологический элемент, на котором возникла ошибка.
#endif
            class DLLEXPORT GeneralTopolError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the topological item with the error
                /// <returns>Topological item</returns>
#else
                /// Получить топологический элемент, на котором возникает ошибка
                /// <returns>Указатель на топологический элемент</returns>
#endif
                Model::TopolPtr GetTopol() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Получить строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Получить набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

                //DOM-IGNORE-BEGIN
            public:
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol">Элемент на котором произошла ошибка</param>
                /// <returns></returns>
                GeneralTopolError(ErrorType iError, Model::TopolPtr iTopol);

                /// Создаёт ошибку и возвращает ссылку на неё
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iTopol">Элемент на котором произошла ошибка</param>
                /// <returns>ссылка на ошибку</returns>
                static GeneralTopolErrorPtr Create(ErrorType iError, Model::TopolPtr iTopol);
            private:
                Model::TopolPtr _topol;
                //DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
			/// <summary>Class of an error is concerned with a correctness of shell nesting</summary>
            /// Class of an error is concerned with  a correctness of shell nesting. Keeps a reference to two shells with an incorrect nesting.
#else
            /// <summary>Класс ошибки, связанной с корректностью вложенности оболочек</summary>
            /// Класс ошибки, связанной с корректностью вложенности оболочек. Хранит ссылки на две оболочки, которые имеют некорректную вложенность.
#endif
            class DLLEXPORT ShellsCompatibilityError : public CheckError
            {
            public:
#ifdef ENGLISH_HELP
				/// Get a pointer to the first shell
                /// <returns>The first shell</returns>
#else
                /// Возвращает первую оболочку
                /// <returns>Первая оболочка</returns>
#endif
                Model::ShellPtr GetFirstShell() const;

#ifdef ENGLISH_HELP
				/// Get a pointer to the second shell
                /// <returns>The second shell</returns>
#else
                /// Возвращает вторую оболочку
                /// <returns>Вторая оболочка</returns>
#endif
                Model::ShellPtr GetSecondShell() const;

#ifdef ENGLISH_HELP
				/// /// Get a string describing the error has been found by model check
                /// <returns>String describing the error</returns>
#else
                /// Возвращает строку описания ошибки, выявленной при проверке модели
                /// <returns>Строка ошибки</returns>
#endif
                virtual Common::String ToString() const override;

#ifdef ENGLISH_HELP
				/// Get a set of topological objects the error has been created for
#else
                /// Возвращает набор объектов модели, для которых создан объект ошибки
#endif
                virtual void GetTopols(std::vector<Model::TopolPtr>& oTopols) const override;

                //DOM-IGNORE-BEGIN
            public: 
                /// Конструктор
                /// <param name="iError">Тип ошибки</param>
                /// <param name="iShell1">первое R-ребро</param>
                /// <param name="iShell2">второе R-ребро</param>
                /// <returns></returns>
                ShellsCompatibilityError(ErrorType iError, Model::ShellPtr iShell1, Model::ShellPtr iShell2);

                /// Создаёт ошибку и возвращает на неё ссылку
                /// <param name="iError">Тип Ошибки</param>
                /// <param name="iShell1">Первая оболочка</param>
                /// <param name="iShell2">Вторая оболочка</param>
                /// <returns></returns>
                static ShellsCompatibilityErrorPtr Create(ErrorType iError, Model::ShellPtr iShell1, Model::ShellPtr iShell2);

            private:
                Model::ShellPtr _shell1;
                Model::ShellPtr _shell2;
                //DOM-IGNORE-END
            };
        }
    }
}

