﻿// ClashCalculator.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c) Авторское право 2012 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание класса Tools::ClashCalculator
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "decl.h"
#include "forward.h"

#include "Common/BaseTools.h"
#include "Math/AffineMap3D.h"

namespace RGK
{
    namespace Tools
    {
#ifdef ENGLISH_HELP
		/// Types of clashes
        /// <seealso cref="Clash detection"/>
#else
        /// Типы столкновений
        /// <seealso cref="Столкновения"/>
#endif
        enum ClashType
        {
#ifdef ENGLISH_HELP
			/// There are no any clashes
#else
            /// Столкновения нет
#endif
            None = 0,
#ifdef ENGLISH_HELP 
			/// Clash is found
#else
            /// Столкновение найдено, тип столкновения детализировать не требуется
#endif
            ClashExists,
#ifdef ENGLISH_HELP
			/// The second element contains the first one wholly.
#else
            /// Первый элемент целиком содержится во втором
#endif
            FirstInSecond,
#ifdef ENGLISH_HELP 
			/// The first element contains the second one wholly.
#else
            /// Второй элемент целиком содержится в первом
#endif
            SecondInFirst,
#ifdef ENGLISH_HELP 
			/// Penetration 
#else
            /// Проникновение
#endif
            Interfere,
#ifdef ENGLISH_HELP
			/// One element abuts on other one. Type of contact is not matter.
#else
            /// Примыкание, тип примыкания устанавливать не требуется
#endif
            AbutNoClass,
#ifdef ENGLISH_HELP
			/// Joining by an inner manner. The second element contains the first one
#else
            /// Примыкание внутренним образом, первый элемент содержится во втором
#endif
            AbutFirstInSecond,
#ifdef ENGLISH_HELP
			/// Joining by an inner manner. The first element contains the second one
#else
            /// Примыкание внутренним образом, второй элемент содержится в первом
#endif
            AbutSecondInFirst,
#ifdef ENGLISH_HELP
			/// Joining by an outer manner.
#else
            /// Примыкание внешним образом
#endif
            AbutOut,
#ifdef ENGLISH_HELP
			/// Can not specify a type of a clash
#else
            /// Тип столкновения установить не удалось
#endif
            Unknown
        };

#ifdef ENGLISH_HELP 
        /// Class for a seeking of clashes between two sets of topological items of a model
        /// <seealso cref="Clash detection"/>
#else
        /// Класс для нахождения столкновений между двумя наборами топологических элементов модели
        /// <seealso cref="Столкновения"/>
#endif
        class DLLEXPORT ClashCalculator : public Common::BaseTools
        {
        public:

#ifdef ENGLISH_HELP 
            /// <summary>Constructor</summary>
            /// <param name="iContext">Context of the operation</param>
#else
            /// <summary>Конструктор</summary>
            /// <param name="iContext">Контекст вычисления</param>
#endif
            ClashCalculator(Common::Context* iContext);

#ifdef ENGLISH_HELP
			/// <summary>Destructor</summary>
#else
            /// <summary>Деструктор</summary>
#endif
            ~ClashCalculator();

#ifdef ENGLISH_HELP 
            /// Data for the clash seeking.
#else
            /// Класс постановки задачи нахождения столкновений между двумя наборами топологических элементов модели
#endif
            class DLLEXPORT Data
            {
            public:

#ifdef ENGLISH_HELP
				/// Group of elements for the clash calculation
#else
                /// Группа элементов, для которых выполняется проверка столкновения
#endif
                class DLLEXPORT Group
                {
                public:

#ifdef ENGLISH_HELP
					/// Topological element for the clash seeking
#else
                    /// Топологический элемент, для которого выполняется проверка столкновения
#endif
                    class DLLEXPORT Element
                    {
                    public:
#ifdef ENGLISH_HELP
                        /// Constructor
                        /// <param name="iTopol">Topological element</param>
                        /// <param name="iMap">Ortogonal mapping for the element</param>
#else
                        /// Конструктор
                        /// <param name="iTopol">Топологический элемент</param>
                        /// <param name="iMap">Ортогональное преобразование, применяемое к элементу</param>
#endif
                        Element(const Model::TopolPtr& iTopol, const Math::AffineMap3D& iMap=Math::AffineMap3D()):_topol(iTopol),_map(iMap){}

#ifdef ENGLISH_HELP
                        /// Get an ortogonal mapping of the element
                        /// <returns>Affine map</returns>
#else
                        /// Ортогональное преобразование, применяемое к элементу
                        /// <returns>Ортогональное преобразование, применяемое к элементу</returns>
#endif
                        const Math::AffineMap3D& GetMap()const { return _map; }

#ifdef ENGLISH_HELP 
                        /// Get the topological element for the clash seeking
                        /// <returns>
                        /// Topological element
                        /// </returns>
#else
                        /// Получить топологический элемент, для которого выполняется проверка столкновения
                        /// <returns>
                        /// Топологический элемент, для которого выполняется проверка столкновения
                        /// </returns>
#endif
                        const Model::TopolPtr& GetTopol()const { return _topol; }

#ifdef ENGLISH_HELP
						/// Destructor
#else
                        /// Деструктор
#endif
                        ~Element(){}

//DOM-IGNORE-BEGIN
                    private:
                        Model::TopolPtr _topol;
                        Math::AffineMap3D _map;

                        friend class ClashCalculator;
//DOM-IGNORE-END
                    };

#ifdef ENGLISH_HELP
					/// Shared pointer to an topological element
#else
                    /// Класс, обеспечивающий хранение ссылки на элемент
#endif
                    typedef std::shared_ptr<const Element> ElementPtr;

#ifdef ENGLISH_HELP
					/// Array of topological elements
#else
                    /// Множество элементов
#endif
                    typedef std::vector<ElementPtr> ElementArray;

#ifdef ENGLISH_HELP
					/// Constructor
#else
                    /// Конструктор
#endif
                    Group();

#ifdef ENGLISH_HELP 
                    /// Constructor
                    /// <param name="iElement">Element</param>
#else
                    /// Конструктор
                    /// <param name="iElement">Элемент</param>
#endif
                    Group(const ElementPtr& iElement);

#ifdef ENGLISH_HELP 
                    /// Constructor
                    /// <param name="iElements">Array of elements</param>
#else
                    /// Конструктор
                    /// <param name="iElements">Множество элементов</param>
#endif
                    Group(const ElementArray& iElements);

#ifdef ENGLISH_HELP
					/// Destructor
#else
                    /// Деструктор
#endif
                    ~ Group ();

#ifdef ENGLISH_HELP 
                    /// Add a set of elements
                    /// <param name="iElements">Array of elements</param>
                    /// <returns>
                    /// - Result::Success in the case of a success
                    ///</returns>
#else
                    /// Добавить множество элементов
                    /// <param name="iElements">Множество элементов</param>
                    /// <returns>
                    /// - Result::Success в случае успешного выполнения
                    ///</returns>
#endif
                    Common::Result AppendElements(const ElementArray& iElements);

#ifdef ENGLISH_HELP 
                    /// Add a topological element
                    /// <param name="iElement">Element</param>
                    /// <returns>
                    /// - Result::Success in the case of a success
                    ///</returns>
#else
                    /// Добавить топологический элемент
                    /// <param name="iElement">Элемент</param>
                    /// <returns>
                    /// - Result::Success в случае успешного выполнения
                    ///</returns>
#endif
                    Common::Result AddElement(const ElementPtr& iElement);

#ifdef ENGLISH_HELP 
                    /// Get a set of elements
                    /// <returns>
                    /// Element array
                    ///</returns>
#else
                    /// Получить множество элементов
                    /// <returns>
                    /// Множество элементов
                    ///</returns>
#endif
                    const ElementArray& GetElements() const { return _topols; }

//DOM-IGNORE-BEGIN
                private:

                    ElementArray _topols; // Множество элементов

                    friend class ClashCalculator;
//DOM-IGNORE-END
                };

#ifdef ENGLISH_HELP 
                /// Shared pointer to the group of elements
#else
                /// Класс, обеспечивающий хранение ссылки на группу элементов
#endif
                typedef std::shared_ptr<const Group> GroupPtr;

#ifdef ENGLISH_HELP
                /// Constructor
                /// <param name="iGroup1">First gtoup of elements</param>
                /// <param name="iGroup2">Second gtoup of elements</param>
#else
                /// Конструктор
                /// <param name="iGroup1">Первая группа элементов</param>
                /// <param name="iGroup2">Вторая группа элементов</param>
#endif
                Data(const GroupPtr& iGroup1,const GroupPtr& iGroup2);

#ifdef ENGLISH_HELP
				/// Destructor
#else
                /// Деструктор
#endif
                ~ Data ();

#ifdef ENGLISH_HELP 
                /// Get the first group of elements
                /// <returns>
                /// Group of elements
                ///</returns>
#else
                /// Получить первую группу элементов
                /// <returns>
                /// Первая группа элементов
                ///</returns>
#endif
                const GroupPtr& GetGroup1() const { return _group1; }

#ifdef ENGLISH_HELP
				/// Get the second group of elements
                /// <returns>
                /// Group of elements
                ///</returns>
#else
                /// Получить вторую группу элементов
                /// <returns>
                /// Вторая группа элементов
                ///</returns>
#endif
                const GroupPtr& GetGroup2() const { return _group2; }

#ifdef ENGLISH_HELP 
                /// Set flugs of a clash definition by an owning body
                /// <param name="iDefineClashByBodyA">Boolean flag for the first group of elements</param>
                /// <param name="iDefineClashByBodyB">Boolean flag for the second group of elements</param>
#else
                /// Установить флаги определения столкновений по владеющему телу
                /// <param name="iDefineClashByBodyA">Флаг для первой группы элементов</param>
                /// <param name="iDefineClashByBodyB">Флаг для второй группы элементов</param>
#endif
                void SetClashDetectionByOwningBody(bool iDefineClashByBody1, bool iDefineClashByBody2);

#ifdef ENGLISH_HELP
                /// Set the flag of a clash types definition
                /// <param name="iDefineClashTypes">Boolean flag</param>
#else
                /// Установить флаг определения типов столкновений
                /// <param name="iDefineClashTypes">Флаг определения типов столкновений</param>
#endif
                void SetClashTypeDefinitionMode(bool iDefineClashTypes);

#ifdef ENGLISH_HELP 
                /// Set the flag of seeking of all clashes are minimal by topologies are included
                /// <param name="iFindAllClashes">Boolean flag</param>
#else
                /// Установить флаг поиска всех столкновений минимальных по включению топологий
                /// <param name="iFindAllClashes">Флаг поиска всех столкновений минимальных по включению топологий</param>
#endif
                void SetFindAllClashesMode(bool iFindAllClashes);

//DOM-IGNORE-BEGIN
            private:

                GroupPtr _group1, _group2;
                bool _defineClashesByBody1;
                bool _defineClashesByBody2;

                bool _defineClashTypes;
                bool _findAllClashes;

                friend class ClashCalculator;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
			/// Class of clash seeking results between two sets of model topological elements
#else
            /// Класс результатов решения задачи нахождения столкновений между двумя наборами топологических элементов модели
#endif
            class DLLEXPORT Report
            {
            public:
#ifdef ENGLISH_HELP
				/// Constructor
#else
                /// Конструктор
#endif
                Report();

#ifdef ENGLISH_HELP 
				/// Destructor
#else
                /// Деструктор
#endif
                ~Report();

#ifdef ENGLISH_HELP
				/// Structure with data about clash between two topological elemnts of a model
#else
                /// Структура, хранящая данные о столкновении между двумя топологическими элементами модели.
#endif
                struct DLLEXPORT ClashDescription
                {
                public:
#ifdef ENGLISH_HELP 
                    /// Get a topological subelement the contact of a first group element is performed by
                    /// <returns>
                    /// Topological item
                    /// </returns>
#else
                    /// Получить топологический подэлемент, по которому выполняется контакт элемента первой группы
                    /// <returns>
                    /// Топологический подэлемент, по которому выполняется контакт элемента первой группы
                    /// </returns>
#endif
                    const Model::TopolPtr& GetTopol1() const { return _topol1; }
#ifdef ENGLISH_HELP 
					/// Get a topological subelement the contact of a second group element is performed by
                    /// <returns>
                    /// Topological item
                    /// </returns>
#else
                    /// Получить топологический подэлемент, по которому выполняется контакт элемента второй группы
                    /// <returns>
                    /// Топологический подэлемент, по которому выполняется контакт элемента второй группы
                    /// </returns>
#endif
                    const Model::TopolPtr& GetTopol2() const { return _topol2; }
#ifdef ENGLISH_HELP 
                    /// Get an index of a topological element of the first group
                    /// <returns>
                    /// Index
                    /// </returns>
#else
                    /// Получить индекс топологического элемента первой группы
                    /// <returns>
                    /// Индекс топологического элемента первой группы
                    /// </returns>
#endif
                    unsigned int GetElement1() const { return _idxOwner1; }
#ifdef ENGLISH_HELP 
                    /// Get an index of a topological element of the second group
                    /// <returns>
                    /// Index
                    /// </returns>
#else
                    /// Получить индекс топологического элемента второй группы
                    /// <returns>
                    /// Индекс топологического элемента второй группы
                    /// </returns>
#endif
                    unsigned int GetElement2() const { return _idxOwner2; }

#ifdef ENGLISH_HELP 
                    /// Get a type of the clash
                    /// <returns>
                    /// Type of the clash
                    /// </returns>
#else
                    /// Получить тип столкновения
                    /// <returns>
                    /// Тип столкновения
                    /// </returns>
#endif
                    ClashType GetType() const { return _clashType; }

//DOM-IGNORE-BEGIN
                private:
                    ClashDescription();
                    ClashDescription(const Model::TopolPtr& iTopol1, const Model::TopolPtr& iTopol2, unsigned int  _Element1, unsigned int  _Element2, ClashType iClashType);

                    bool IsEqual(const ClashDescription& iOtherClashDescr) const;

                    Model::TopolPtr _topol1;
                    int  _idxOwner1;
                    Model::TopolPtr _topol2;
                    int  _idxOwner2;
                    ClashType _clashType;

                    friend class ClashCalculator;
                    friend class ClashCalculatorTools;
                    friend class ReplayUtilities::ClashCalculatorTestOperator;
//DOM-IGNORE-END
                };

#ifdef ENGLISH_HELP
				/// Array of clashes
#else
                /// Список столкновений
#endif
                typedef std::vector<ClashDescription> ClashArray;

#ifdef ENGLISH_HELP 
                /// Get a list of clashes
                /// <returns>
                /// Array of clashes
                ///</returns>
#else
                /// Получить список столкновений
                /// <returns>
                /// Список столкновений
                ///</returns>
#endif
                const ClashArray& GetClashes() const { return _clashes; }

#ifdef ENGLISH_HELP 
                /// Get a total clash type
                /// <returns>
                /// Type of a clash
                ///</returns>
#else
                /// Получить итоговый тип столкновения
                /// <returns>
                /// Тип столкновения
                ///</returns>
#endif
                ClashType GetTotalClashType() const {return _totalClashType;}

//DOM-IGNORE-BEGIN
            private:

                ClashArray _clashes;
                ClashType _totalClashType;

                void AddClash(ClashDescription iClash) {_clashes.push_back(iClash);}
                void SetTotalClashType(ClashType iClashType) {_totalClashType = iClashType;}

                friend class ClashCalculator;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP 
			/// Run a clash calculation
            /// <param name="iData">Initial data for the operation</param>
            /// <param name="oReport">Report with results of the operation</param>
            /// <returns>
            /// Result of the call:
            /// - Common::Success in the case of a success
            ///</returns>
#else
            /// Выполнить поиск столкновений
            /// <param name="iData">Исходные данные для выполнения операции</param>
            /// <param name="oReport">Отчёт о результатах выполнения операции</param>
            /// <returns>
            /// Результат выполнения операции:
            /// - Common::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result Run(const Data& iData, Report& oReport);

//DOM-IGNORE-BEGIN
        private:
//DOM-IGNORE-END
        };

    }
}
