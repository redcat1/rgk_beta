﻿// Context.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание класса RGK::Common::Context
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include <memory>
#include <vector>
#include <map>

#include "decl.h"
#include "forward.h"

#include "Model/Synchronization.h"
#include "Common/ReferenceCounting.h"
#include "Model/EntityIterator.h"

namespace RGK
{
	namespace Common
	{
#ifdef ENGLISH_HELP
        /// <summary>Calculation context of the model</summary>
        /// <remarks>
        /// Context is passed to all the functions dealing with the model.
        /// Context is used for solving the following problems:
        /// - keeping records of changes in the current model;
        /// - synchronization for multithread calculations;
        /// - access to the active data set (session) of the kernel;
        /// - obtaining tolerance parameters and other kernel settings.
        /// </remarks>
        /// <seealso cref="Session and context"/>
#else
        /// <summary>Контекст вычислений в модели</summary>
        /// <remarks>
		/// Контекст передаётся всем функциям, работающим с моделью.
		/// Контекст используется для решения следующих задач:
		/// - Протоколирование изменений в модели;
		/// - Синхронизация параллельных вычислений;
        /// - Доступ к активному набору данных (сессии) ядра;
		/// - Получение параметров точности и других настроек ядра;
        /// </remarks>
        /// <seealso cref="Сессия модели и контекст"/>
#endif
		class DLLEXPORT Context : public ReferenceCounting
		{
		public:
#ifdef ENGLISH_HELP
            /// Get the context session
            /// <returns>Context session</returns>
#else
			/// Получить сессию контекста
			/// <returns>Сессия контекста</returns>
#endif
			Model::Session* GetSession() const { return _session; }

#ifdef ENGLISH_HELP
            /// Check if the parts can be locked.
            /// The array of bodies being modified and the array of bodies being used for calculations shouldn't
            /// share any common elements.
            /// <param name="iModified">Array of bodies expected to be modified</param>
            /// <param name="iCalculated">Array of bodies used for calculations</param>
            /// <returns>
            /// - Result::Success in case, when the locking is possible
            /// - Result::CannotCalculateModifiedBody the body can't be locked for calculations, for it has been locked for modification
            /// - Result::CannotModifyModifiedBody the body can't be locked for modification, for it has been locked for modification in another context
            /// - Result::CannotModifyCalculatedBody the body can't be locked for modification, for it has been locked for calculations
            /// </returns>
#else
            /// Проверка возможности блокировки.
            /// Множество модифицируемых тел и множество тел, которые используются для вычислений, не должны иметь общих элементов
            /// <param name="iModified">Список тел, предполагаемых к модификации</param>
            /// <param name="iCalculated">Список тел, используемых для вычислений</param>
            /// <returns>
            /// - Result::Success в случае, если блокировка возможна
            /// - Result::CannotCalculateModifiedBody тело нельзя блокировать для вычислений, так как оно заблокировано для редактирования
            /// - Result::CannotModifyModifiedBody Тело нельзя блокировать для редактирования, так как оно заблокировано для редактирования в другом контексте.
            /// - Result::CannotModifyCalculatedBody Тело нельзя блокировать для редактирования, так как оно заблокировано для вычислений.
            ///</returns>
#endif
            Result CanLockParts(const Model::PartArray& iModified, const Model::PartArray& iCalculated);

#ifdef ENGLISH_HELP
            /// Locking of a group of bodies in a single operation.
            /// The array of bodies being modified and the array of bodies being used for calculations shouldn't
            /// share any common elements.
            /// <param name="iModified">Array of bodies expected to be modified</param>
            /// <param name="iCalculated">Array of bodies used for calculations</param>
            /// <returns>
            /// - Result::Success in case, when the locking has been done successfully
            /// - Result::CannotCalculateModifiedBody the body can't be locked for calculations, for it has been locked for modification
            /// - Result::CannotModifyModifiedBody the body can't be locked for modification, for it has been locked for modification in another context
            /// - Result::CannotModifyCalculatedBody the body can't be locked for modification, for it has been locked for calculations
            /// </returns>
#else
            /// Блокировка группы тел одной транзакцией.
            /// Множество модифицируемых тел и множество тел, которые используются для вычислений, не должны иметь общих элементов
            /// <param name="iModified">Список тел, предполагаемых к модификации</param>
            /// <param name="iCalculated">Список тел, используемых для вычислений</param>
            /// <returns>
            /// - Result::Success в случае, если блокировка выполнена
            /// - Result::CannotCalculateModifiedBody тело нельзя блокировать для вычислений, так как оно заблокировано для редактирования
            /// - Result::CannotModifyModifiedBody Тело нельзя блокировать для редактирования, так как оно заблокировано для редактирования в другом контексте.
            /// - Result::CannotModifyCalculatedBody Тело нельзя блокировать для редактирования, так как оно заблокировано для вычислений.
            ///</returns>
#endif
            Result LockParts(const Model::PartArray& iModified, const Model::PartArray& iCalculated);

#ifdef ENGLISH_HELP
            /// Unlocking of a group of bodies in a single operation. It is required that the group has already been locked in the current context.
            /// <param name="iParts">Array of bodies which have previously been in the current context</param>
            /// <returns>
            /// - Result::Success in case, when the unlocking has been done successfully
            /// - Result::CannotUnlockBody the body hasn't been locked by the context or the number of unlocks exceeds the number of locks
            ///</returns>
#else
            /// Разблокировка группы ранее заблокированных в данном контексте тел одной транзакцией
            /// <param name="iParts">Список ранее заблокированных в данном контексте тел</param>
            /// <returns>
            /// - Result::Success в случае, если разблокировка выполнена
            /// - Result::CannotUnlockBody Тело не было блокировано контекстом или количество разблокировок больше количества блокировок.
            ///</returns>
#endif
            Result UnlockParts(const Model::PartArray& iParts);

#ifdef ENGLISH_HELP
            /// <summary>Linear precision of the kernel session</summary>
            /// Linear precision of the kernel session is used:
            /// - when performing mathematical calculations associated with numerical analysis
            /// - for value comparison
            /// - when comparing linear dimensions to null
            /// <returns>Linear precision of the current kernel session</summary>
#else
            /// <summary>Линейная точность сессии ядра</summary>
			/// Линейная точность сессии ядра используется при математических вычислениях, связанных с использованием численных методов, 
			/// при сравнении значений между собой, а также при сравнении линейных размеров с нулевым значением
			/// <returns>Линейная точность вычислений в текущей сессии ядра</returns>
#endif
            double GetLinearPrecision () const { return _linearPrecision; }

#ifdef ENGLISH_HELP
            /// <summary>Angular precision of the kernel session</summary>
            /// Angular precision of the kernel session is used:
            /// - when performing mathematical calculations associated with numerical analysis
            /// - for angular value comparison
            /// - when comparing angles to null
            /// <returns>Angular precision of the current kernel session</summary>
#else
            /// <summary>Угловая точность сессии ядра</summary>
            /// Угловая точность сессии ядра используется при математических вычислениях, связанных с использованием численных методов, 
			/// при сравнении угловых значений между собой, а также при сравнении значений углов с нулевым значением
			/// <returns>Угловая точность вычислений в текущей сессии ядра</returns>
#endif
            double GetAngularPrecision () const { return _angularPrecision; }

#ifdef ENGLISH_HELP
            /// Relative precision. It is used to compare intervals in parameter space of curves and surfaces.
            /// Parameters differ with accuracies accumulated due to computer arithmetic.
#else
            /// Относительная точность. Используется для сравнения интервалов в параметрической области кривых и поверхностей.
            /// Параметры различаются с точностью до погрешностей, накапливаемых из-за машинной арифметики
#endif
            double GetUnitPrecision () const { return _unitPrecision; }

#ifdef ENGLISH_HELP
            /// <summary>Maximum model dimensions</summary>
            /// Maximum model dimensions in a session are limited in terms of a chosen system of units.
            /// The standard units of measurement are meters.
            /// This method returns a value which is equal to a half of the size of the cube, placed in
            /// the global coordinate system so that it's center coincides (0, 0, 0).
            /// <returns>Maximum model dimensions in the current kernel version</returns>
#else
            /// <summary>Максимально допустимые габариты модели</summary>
			/// Максимальные габариты модели в сессии ограничены в выбранных единицах измерения.
			/// Стандартными единицами измерения являются метры. 
			/// Данный метод возвращает значение, равное половине размера куба, размещённого в глобальной системе координат 
			/// с центром в точке (0, 0, 0)
			/// <returns>Максимально допустимые габариты модели в текущей сессии ядра</returns>
#endif
            double GetSizeBox () const { return _sizeBox; }

#ifdef ENGLISH_HELP
            /// Lock the session from the context.
            /// Calling this function allows to stop the execution of the kernel in other threads.
#else
            /// Блокировать сессию из контекста. 
            /// Вызов функции позволяет остановить выполнение ядра в других потоках.
#endif
            void SetLock();

#ifdef ENGLISH_HELP
            /// Unlock the session from the context.
#else
            /// Разблокировать сессию из контекста
#endif
            void UnsetLock();

#ifdef ENGLISH_HELP
            /// Set the maximum number of threads that can be run for this context in the kernel.
            /// <param name="iMaxThreads"> Maximum number of threads that can be run for this context in the kernel</param>
#else
            /// Установить максимальное количество потоков, которое может запускаться внутри ядра для данного контекста
            /// <param name="iMaxThreads">Максимальное количество потоков, которое может запускаться внутри ядра для данного контекста</param>
#endif
            Common::Result SetMaxThreads(unsigned int iMaxThreads);

#ifdef ENGLISH_HELP
            /// Get the maximum number of threads that can be run for this context in the kernel.
            /// <returns> Maximum number of threads that can be run for this context in the kernel</returns>
#else
            /// Получить максимальное количество потоков, которое может запускаться внутри ядра для данного контекста
            /// <returns>Максимальное количество потоков, которое может запускаться внутри ядра для данного контекста</returns>
#endif
            unsigned int GetMaxThreads() const;

//DOM-IGNORE-BEGIN

            /// Открыть новый журнал изменений в контексте
            /// <param name="oState">Метка текущего состояния контекста, перед началом изменений. Метка становится неактуальной(точнее переносится в сессиию) при разблокировании тел. </param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
            Common::Result BeginChanges (ContextState& oState);

            /// Откатить изменения в модели до метки
            /// <param name="iState">Метка, до которой выполняется откат. Все изменения удаляются. То есть повторить изменения не получится</param>
            Common::Result Undo(const ContextState& iState);

            /// Подавить вывод диагностики об ошибках
            /// <param name="iSuppress">Подавить вывод диагностики об ошибках</param>
            /// <returns>Предыдущее значение</returns>
            bool SuppressErrorChecking(bool iSuppress);

            /// Получить состояние подавления вывода диагностики об ошибках
            /// <returns>Подавить вывод диагностики об ошибках</returns>
            bool IsErrorCheckingSuppressed() const { return _suppressErrorChecking; }

		private:
			Context(Model::Session* session);
            ~ Context();

            void Finalize();

            /// Получить текущий журнал изменений, в который добавляются изменения. Может возвращаться 0, если изменения не протоколируются
            /// <returns>Текущий журнал изменений</returns>
            Model::ChangeLogPtr GetChangeLog();

            /// Список граней
            typedef Model::ElementsList<Model::ChangeLogPtr> ChangeList;
            ChangeList GetChanges() const { return ChangeList(_firstChange); }

        private:
            RGK::Model::Session* _session;
            typedef std::map<Model::PartPtr,int> BodyLockArray;
            BodyLockArray _modified; // Список тел, блокированных в данном контесте для редактирования
            BodyLockArray _calculated; // Список тел, блокированных в данном контесте для вычислений
            Model::ChangeLogPtr _firstChange; // Общий журнал изменений для всех тел. Предполагается последовательное обращение к протоколу из одного потока

            double _linearPrecision; /// Линейная точность
            double _angularPrecision; /// Угловая точность
            double _unitPrecision; /// Относительная точность
            double _sizeBox; /// Максимально допустимые габариты модели

            unsigned int _maxThreads; /// Максимальное количество потоков

            bool _journalling; // Вести журнал изменений

            bool _suppressErrorChecking; /// Подавить вывод диагностики об ошибках

			friend class Model::Session;
            friend class UsingContext;
            friend class Generators::TopologyModifier;
//DOM-IGNORE-END
		};

#ifdef ENGLISH_HELP
        /// A utility class for the context using
        /// <remarks>
        /// When the using of this utility class finishes, the class releases the context automatically.
        /// It is handy to create class instances as automatic variables when entering a method or function.
        /// When the execution leaves the method or function, the class destructor is called which leads
        /// to automatic release of the context. Thereby manual use of Lock and Unlock becomes inessential.
        /// </remarks>
#else
        /// Вспомогательный класс для использования контекста
        /// <remarks>
        /// Инструментальный класс для автоматического освобождения контекста по завершении использования. 
		/// Объекты данного класса удобно создавать в виде автоматических переменных при входе в метод или функцию.
		/// При выходе из функции или метода, при вызове деструктора объекта, контекст автоматически освобождается.
		/// Таким образом, отпадает необходимость ручного вызова методов Lock, Unlock
        /// </remarks>
#endif
        class DLLEXPORT UsingContext
        {
        public:
#ifdef ENGLISH_HELP
            /// Constructor
            /// <param name="iContext">Context</param>
            /// <param name="lock">Indicates the need for an automatic lock of the context directly inside the constructor</param>
#else
			/// Конструктор
            /// <param name="iContext">Контекст</param>
            /// <param name="lock">Необходимость автоматической блокировки контекста непосредственно в конструкторе</param>
#endif
            UsingContext(Context* iContext, bool lock = false);

#ifdef ENGLISH_HELP
            /// Destructor
#else
			/// Деструктор
#endif
			virtual ~UsingContext();
            
         public:

#ifdef ENGLISH_HELP
            /// Operator to cast to Context
#else
			/// Оператор приведения объекта к типу Context
#endif
            operator Context* () const { return _context; }

#ifdef ENGLISH_HELP
            /// Operator to cast to Context
#else
			/// Оператор приведения объекта к типу Context
#endif
            Context* operator-> () const { return _context; }

#ifdef ENGLISH_HELP
            /// Get the context
#else
			/// Получить контекст
#endif
            Context* GetContext() const { return _context; }

//DOM-IGNORE-BEGIN
        private:
            UsingContext(const UsingContext&){_context=nullptr;}
            UsingContext& operator=(const UsingContext& context){ _context = context._context; return *this; };
        protected:
            Context* _context;
//DOM-IGNORE-END
        };

#ifdef ENGLISH_HELP
        /// A utility class allowing to create the context and to release the context automatically
        /// when it is destroyed.
#else
        /// Инструментальный класс для создания и автоматического освобождения контекста по завершении использования
#endif
        class DLLEXPORT ThreadContext : public UsingContext
        {
        public :
#ifdef ENGLISH_HELP
            /// Constructor
            /// <param name="iSession">Session</param>
#else
			/// Конструктор
            /// <param name="iSession">Сессия</param>
#endif
            ThreadContext(Model::Session* iSession);

#ifdef ENGLISH_HELP
            /// Destructor
#else
			/// Деструктор
#endif
            virtual ~ThreadContext() {}
//DOM-IGNORE-BEGIN
        private:
            ThreadContext(const ThreadContext&):UsingContext(nullptr){}
            ThreadContext& operator=(const ThreadContext&){return *this;};
//DOM-IGNORE-END
        };

#ifdef ENGLISH_HELP
        /// A utility class allowing to create the context of the main thread and
        /// to release the context of the main thread automatically
#else
        /// Инструментальный класс для создания и автоматического освобождения контекста основного потока
#endif
        class DLLEXPORT MainContext : public UsingContext
        {
        public :
#ifdef ENGLISH_HELP
            /// Constructor
            /// <param name="iSession">Session</param>
#else
			/// Конструктор
            /// <param name="iSession">Сессия</param>
#endif
            MainContext(Model::Session* iSession);

#ifdef ENGLISH_HELP
            /// Destructor
#else
			/// Деструктор
#endif
            virtual ~MainContext() {}
//DOM-IGNORE-BEGIN
        private:
            MainContext(const MainContext&):UsingContext(nullptr){}
            MainContext& operator=(const ThreadContext&){return *this;};
//DOM-IGNORE-END
        };
	}
}
