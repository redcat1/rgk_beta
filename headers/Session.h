﻿// Session.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание класса Model::Session
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include <set>
#include <vector>
#include <omp.h>
#include <map>

#include "decl.h"
#include "forward.h"
#include "ReplayUtilities/FunctionalityType.h"
#include <FileSystem/path.hpp>

namespace RGK
{
    namespace Common
    {
        class Version;
    }

    namespace Model
	{
        class SessionState;
        template<class T> class ElementsList;

#ifdef ENGLISH_HELP
        /// <summary>Kernel session</summary>
        /// A kernel session stores an isolated set of system settings (calculation precision parameters, maximal sizes, etc.) and all the bodies, created in it.
        /// Every model object belongs to a certain session. If a session is destroyed, all the model objects belonging to it are destroyed automatically.
        /// <seealso cref="Session and context"/>
#else
        /// <summary>Сессия ядра</summary>
        /// Сессия ядра хранит изолированный набор системных установок (параметров точности вычислений, максимальных габаритов и т.д.) и все тела, созданные в ней.
		/// Любой объект модели принадлежит определённой сессии. При удалении сессии все объекты модели,
		/// принадлежащие ей, автоматически удаляются.
        /// <seealso cref="Сессия модели и контекст"/>
#endif
		class DLLEXPORT Session
		{
		public:
#ifdef ENGLISH_HELP
            /// <summary>A constructor creating a session with default precisions and an appropriate maximal size</summary>
#else
            /// <summary>Конструктор, создающий сессию с точностями по умолчанию и соответствующим размером задачи</summary>
#endif
            Session();

#ifdef ENGLISH_HELP
            /// <summary>A constructor creating a session with user-provided precisions; maximal size is calculated as LinearPrecision/AngularPrecision</summary>
            /// <param name="iLinearPrecision">Linear precision</param>
            /// <param name="iAngularPrecision">Angular precision</param>
            /// <param name="iUnitPrecision">Unit precision. It is used to compare intervals in parameter domains of curves and surfaces.
            /// Parameters differ precision accumulated due to computer arithmetic.</param>
#else
            /// <summary>Конструктор, создающий сессию с пользовательскими точностями; размер задачи вычисляется как LinearPrecision/AngularPrecision</summary>
            /// <param name="iLinearPrecision">Линейная точность</param>
            /// <param name="iAngularPrecision">Угловая точность</param>
            /// <param name="iUnitPrecision">Относительная точность. Используется для сравнения интервалов в параметрической области кривых и поверхностей.
            /// Параметры различаются с точностью до погрешностей, накапливаемых из-за машинной арифметики</param>
#endif
            Session(double iLinearPrecision, double iAngularPrecision, double iUnitPrecision);

#ifdef ENGLISH_HELP
            /// Destructor
#else
			/// Деструктор
#endif
			~ Session();

		public:
#ifdef ENGLISH_HELP
            /// <summary>Get the context for performing calculations in the main thread</summary>
            /// Get the context for performing calculations in the main thread.
            /// A context is an instrument for synchronization and it can't be transferred from one thread to another.
            /// It is recommended to use only one context within one thread.
            /// <returns>Main thread context</returns>
#else
			/// <summary>Получить контекст для вычислений в основном потоке</summary>
			/// Получить контекст для вычислений в основном потоке.
			/// Контекст является средством синхронизации и его нельзя передавать из одного потока в другой.
			/// В пределах одного потока предпочтительно использовать один контекст.
			/// <returns>Контекст основного потока</returns>
#endif
			Common::Context* GetMainContext() const;

#ifdef ENGLISH_HELP
            /// <summary>Create a context for performing calculations in a parallel thread</summary>
            /// Create a context for performing calculations in a parallel thread.
            /// A context is an instrument for synchronization and it can't be transferred from one thread to another.
            /// It is recommended to use only one context within one thread.
            /// The method locks the context.
            /// <returns>A context for performing calculations in a parallel thread.</returns>
#else
            /// <summary>Создать контекст для вычислений в параллельном потоке</summary>
			/// Создать контекст для вычислений в параллельном потоке.
			/// Контекст является средством синхронизации и его нельзя передавать из одного потока в другой.
			/// В пределах одного потока предпочтительно использовать один контекст.
            /// Метод блокирует контекст.
			/// <returns>Контекст для вычислений в параллельном потоке</returns>
#endif
			Common::Context* CreateThreadContext();

#ifdef ENGLISH_HELP
            /// <summary>Context release</summary>
            /// Context release. If the context isn't locked anymore, it is destroyed.
            /// The main thread context isn't destroyed, but one do have to call the release method for it too, in order to synchronize the change log.
            /// <param name="iContext">A context to be released</param>
            /// <returns>Error code</returns>
#else
            /// <summary>Освобождение контекста</summary>
			/// Освобождение контекста. Если контекст больше не блокирован, он удаляется.
            /// Контекст основного потока не удаляется, но для него также нужно вызывать функцию освобождения контекста для синхронизации журнала изменений
			/// <param name="iContext">Контекст для освобождения</param>
			/// <returns>Код ошибки</returns>
#endif
            Common::Result ReleaseThreadContext (Common::Context* iContext);

#ifdef ENGLISH_HELP
            /// Repeat session initialization. All the session's bodies and contexts are destroyed.
            /// <returns>Error code</returns>
#else
			/// Повторно инициализировать сессию. Все тела и контексты из сессии удаляются
			/// <returns>Код ошибки</returns>
#endif
            Common::Result ReInit();

#ifdef ENGLISH_HELP
            /// A list of bodies
#else
            /// Список тел
#endif
            typedef ElementsList<BodyPtr> BodyList;

#ifdef ENGLISH_HELP
            /// Get an iterator over the list of bodies
            /// <returns>An iterator over the list of bodies</returns>
#else
            /// Получить итератор по списку тел
            /// <returns>Итератор по списку тел</returns>
#endif
            BodyList GetBodies() const;

#ifdef ENGLISH_HELP
            /// A list of assemblies
#else
            /// Список сборок
#endif
            typedef ElementsList<AssemblyPtr> AssemblyList;

#ifdef ENGLISH_HELP
            /// Get an iterator over the list of root assemblies
            /// <returns>An iterator over the list of assemblies</returns>
#else
            /// Получить итератор по списку корневых сборок
            /// <returns>Итератор по списку сборок</returns>
#endif
            AssemblyList GetAssemblies() const;

#ifdef ENGLISH_HELP
            /// A list of constraints
#else
            /// Список сопряжений
#endif
            typedef ElementsList<Constraints::JointPtr> JointList;

#ifdef ENGLISH_HELP
            /// Get an iterator over the list of constraints
            /// <returns>An iterator over the list of constraints</returns>
#else
            /// Получить итератор по списку сопряжений
            /// <returns>Итератор по списку сопряжений</returns>
#endif
            JointList GetJoints() const;

#ifdef ENGLISH_HELP
            /// Get the linear precision
            /// <returns>Linear precision</returns>
#else
            /// Получить линейную точность
            /// <returns>Линейная точность</returns>
#endif
            double GetLinearPrecision () const;

#ifdef ENGLISH_HELP
            /// Set the linear precision
            /// <param name="precision">Linear precision</param>
#else
			/// Установить линейную точность
            /// <param name="precision">Линейная точность</param>
#endif
            void SetLinearPrecision (double precision);

#ifdef ENGLISH_HELP
            /// Get the angular precision
            /// <returns>Angular precision</returns>
#else
            /// Получить угловую точность
            /// <returns>Угловая точность</returns>
#endif
            double GetAngularPrecision () const;

#ifdef ENGLISH_HELP
            /// Get the unit precision. It is used to compare intervals in parameter domains of curves and surfaces.
            /// Parameters differ by precision accumulated due to computer arithmetic.
            /// <returns>Unit precision</returns>
#else
            /// Получить относительную точность. Используется для сравнения интервалов в параметрической области кривых и поверхностей.
            /// Параметры различаются с точностью до погрешностей, накапливаемых из-за машинной арифметики.
            /// <returns>Относительная точность</returns>
#endif
            double GetUnitPrecision () const;

#ifdef ENGLISH_HELP
            /// Get the size box
#else
            /// Получить максимально допустимые габариты модели
#endif
            double GetSizeBox () const;

#ifdef ENGLISH_HELP
            /// Get the version of the kernel functions, which are used in the session.
            /// By default, the latest function implementation is used.
            /// It is suggested to set the previous kernel version which the compatibility has to be maintained with, explicitly
            /// in cases, when results of executing kernel functions differ between versions.
            /// <returns>The version number</returns>
#else
            /// Получить версию используемых функций ядра, установленную на сессию.
            /// По умолчанию используется последняя реализация функций.
            /// В ряде случаев, если результаты выполнения функций ядра различаются между версиями,
            /// то для обеспечения совместимости версий, предлагается явно устанавливать предыдущую версию ядра,
            /// совместимость с которой нужно обеспечить
            /// <returns>Номер версии</returns>
#endif
            Common::Version GetVersion() const;

#ifdef ENGLISH_HELP
            /// Set the version of the kernel functions which are used in the session.
            /// By default, the latest function implementation is used.
            /// It is suggested to set the previous kernel version which the compatibility has to be maintained with, explicitly
            /// in cases, when results of executing kernel functions differ between versions.
            /// <param name="iVersion">The version number</param>
#else
            /// Установить версию используемых функций ядра на сессию
            /// По умолчанию используется последняя реализация функций.
            /// В ряде случаев, если результаты выполнения функций ядра различаются между версиями,
            /// то для обеспечения совместимости версий, предлагается явно устанавливать предыдущую версию ядра,
            /// совместимость с которой нужно обеспечить
            /// <param name="iVersion">Номер версии</param>
#endif
            void SetVersion(Common::Version iVersion);

#ifdef ENGLISH_HELP
            /// Set/unset non-manifold mode.
            /// By default, non-manifold mode is turned off.
            /// <param name="iNonManifold">Non-manifold mode</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Установить/сбросить режим работы с обобщённой топологией
            /// По умолчанию, режим работы с обобщённой топологией выключен
            /// <param name="iNonManifold">Режим работы с обобщённой топологией</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result SetNonManifold(bool iNonManifold);

#ifdef ENGLISH_HELP
            /// Check if non-manifold mode is turned on.
            /// <returns>
            /// true - if non-manifold mode is turned on
            /// false - if non-manifold mode is turned off
            /// </returns>
#else
            /// Проверить включён ли режим работы с обобщённой топологией
            /// <returns>
            /// true - включён режим работы с обобщённой топологией
            /// false - режим работы с обобщённой топологией не включён
            ///</returns>
#endif
            bool GetNonManifold() const;

#ifdef ENGLISH_HELP
            /// Get an attribute type definition by its name.
            /// <param name="iName">Attribute type definition</param>
            /// <returns>Attribute definition, if it exists</returns>
#else
            /// Найти описание типа атрибута по имени
            /// <param name="iName">Название типа атрибута</param>
            /// <returns>Описание атрибута, если оно существует</returns>
#endif
            AttributeDefinitionPtr FindAttributeDefinition(Common::Context* iContext, const Common::String& iName);

#ifdef ENGLISH_HELP
            /// Create an attribute type definition by its name.
            /// If the attribute type definition already exists, then the existing definition is returned.
            /// <param name="iName">Attribute type definition</param>
            /// <returns>Attribute definition</returns>
#else
            /// Создание типа атрибута по имени.
            /// Если описание атрибута уже существует, то возвращается существующее описание
            /// <param name="iName">Название типа атрибута</param>
            /// <returns>Описание атрибута</returns>
#endif
            AttributeDefinitionPtr AddAttributeDefinition(Common::Context* iContext, const Common::String& iName);

#ifdef ENGLISH_HELP
            /// Get the first attribute in the list
            /// <returns>Value of the attribute</returns>
#else
            /// Получить первый атрибут в списке
            /// <returns>Значение атрибута</returns>
#endif
            const AttributeListPtr& GetAttributes() const { return _attributesList; }

#ifdef ENGLISH_HELP
            /// Add a copy of an existing attribute
            /// <param name="iContext">Context</param>
            /// <param name="iSource">The attribute to be copied</param>
            /// <param name="oCopy">The copy of the attribute which has been added to the session</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Добавить копию существующего атрибута
            /// <param name="iContext">Контекст</param>
            /// <param name="iSource">Копируемый атрибут</param>
            /// <param name="oCopy">Добавленная в объект копия атрибута</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result CopyAttribute(Common::Context* iContext, AttributePtr iSource, AttributePtr& oCopy);

#ifdef ENGLISH_HELP
            /// Delete an attribute
            /// <param name="iContext">Context</param>
            /// <param name="iAttribute">The attribute to be deleted</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            /// - Result::ForeignAttribute in case, when an attribute doesn't belong to the session
            ///</returns>
#else
            /// Удалить атрибут
            /// <param name="iContext">Контекст</param>
            /// <param name="iAttribute">Удаляемый атрибут</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            /// - Result::ForeignAttribute атрибут не принадлежит объекту
            ///</returns>
#endif
            Common::Result DeleteAttribute (Common::Context* iContext, AttributePtr iAttribute);

#ifdef ENGLISH_HELP
            /// Delete an attribute by its definition
            /// <param name="iDefinition">Attribute type definition</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Удалить атрибут по описанию
            /// <param name="iDefinition">Описание типа атрибута</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result DeleteAttribute (Common::Context* iContext, AttributeDefinitionPtr iDefinition);

#ifdef ENGLISH_HELP
            /// Turn the change log storage in the session on/off. Turning the change log off resets the log, if it was turned on.
            /// Management of the change log is available in the case, when there have been no contexts of execution in other threads created.
            /// <param name="iJournalling">true-store the change log</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Включить/выключить хранение журнала изменений в сессии. Выключение журнала сбрасывает журнал, если он был включён.
            /// Управление журналом изменений доступно только если не созданы контексты для выполнения в других потоках.
            /// <param name="iJournalling">true-хранить журнал изменений</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result SetJournalling(Common::Context* iContext, bool iJournalling);

#ifdef ENGLISH_HELP
            /// Check the change log journalling mode
            /// <returns>
            /// - true - the change log journalling mode in turned on
            /// - false - changes are not stored
            /// </returns>
#else
            /// Проверка режима протоколирования журнала изменений в сессии
            /// <returns>
            /// - true - режим протоколирования журнала изменений в сессии
            /// - false - изменения не запоминаются
            /// </returns>
#endif
            bool GetJournalling() const;

#ifdef ENGLISH_HELP
            /// Get the current model state mark
            /// <param name="oState">The model state mark</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Получить метку для текущего состояния модели
            /// <param name="oState">Метка состояния</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result GetState(Common::Context* iContext, SessionState& oState);

#ifdef ENGLISH_HELP
            /// Rollback model changes to a specified state mark.
            /// A roll back is available in case, when there have been no contexts of execution in other threads created.
            /// <param name="iState">The state mark which the changes are rolled back to. All the changes are deleted - it is impossible to repeat them.</param>
            /// <returns>
            /// - Result::Success in case of successful execution
            ///</returns>
#else
            /// Откатить изменения в модели до метки
            /// Откат модели доступен только если не созданы контексты для выполнения в других потоках.
            /// <param name="iState">Метка, до которой выполняется откат. Все изменения удаляются. То есть повторить изменения не получится</param>
            /// <returns>
            /// - Result::Success в случае успешного выполнения
            ///</returns>
#endif
            Common::Result Undo(Common::Context* iContext, const SessionState& iState);

#ifdef ENGLISH_HELP
            /// Set the identification mode of topological elements.
            /// <param name="iIdentify">true-turn on; false-turn off the identification mode of topological elements</param>
#else
            /// Установить режим идентификации топологических элементов
            /// <param name="iIdentify">true-включить;false-выключить режим идентификации топологических элементов</param>
#endif
            void SetIdentify(bool iIdentify);

#ifdef ENGLISH_HELP
            /// Get the identification mode of topological elements.
            /// <returns>The identification mode of topological elements</returns>
#else
            /// Получить режим идентификации топологических элементов
            /// <returns>Режим идентификации топологических элементов</returns>
#endif
            bool GetIdentify() const;

#ifdef ENGLISH_HELP
            /// Set the maximum number of threads which can be run in the kernel for the given context.
            /// <param name="iMaxThreads">The maximum number of threads which can be run in the kernel for the given context</param>
#else
            /// Установить максимальное количество потоков, которое может запускаться внутри ядра для данного контекста
            /// <param name="iMaxThreads">Максимальное количество потоков, которое может запускаться внутри ядра для данного контекста</param>
#endif
            Common::Result SetMaxThreads(unsigned int iMaxThreads);

#ifdef ENGLISH_HELP
            /// Get the maximum number of threads which can be run in the kernel for the given context.
            /// <returns>The maximum number of threads which can be run in the kernel for the given context</returns>
#else
            /// Получить максимальное количество потоков, которое может запускаться внутри ядра для данного контекста
            /// <returns>Максимальное количество потоков, которое может запускаться внутри ядра для данного контекста</returns>
#endif
            unsigned int GetMaxThreads() const;

#ifdef ENGLISH_HELP
            /// Session debug journalling mode
            /// <param name="iUseDebugReport">Turn the session debug journalling mode on/off</param>
            /// <param name="iDebugPath">A path to the folder, where temporary reports are stored</param>
#else
            /// Режим отладочного протоколирования работы сессии
            /// <param name="iUseDebugReport">Включить/выключить режим отладочного протоколирования работы сессии</param>
            /// <param name="iDebugPath">Путь к папке для хранения временных протоколов</param>
#endif
			void UseDebugReport(bool iUseDebugReport, const FileSystem::path& iDebugPath = FileSystem::path());

#ifdef ENGLISH_HELP
            /// Session debug journalling mode
            /// <returns>
            /// - The current session debug journalling mode
            /// </returns>
#else
            /// Режим отладочного протоколирования работы сессии
            /// <returns>
            /// - Текущий режим отладочного протоколирования работы сессии ядра
            ///</returns>
#endif
            bool IsDebugReport() const;

#ifdef ENGLISH_HELP
            /// Set the maximum level of nesting operators
            /// <param name="iLevel">Maximum level of nesting</param>
#else
            /// Установить максимальный уровень вложенности операторов
            /// <param name="iLevel">Максимальный уровень</param>
#endif
            void SetDebugReportMaxLevel(int iLevel);

#ifdef ENGLISH_HELP
            /// Save the debug report to a file
            /// <param name="iContext">Evaluation context</param>
            /// <param name="iFileName">A path to the report file, where temporary reports are packed in</param>
#else
            /// Сохранение отладочного протокола
            /// <param name="iContext">Контекст пересчёта</param>
            /// <param name="iFileName">Путь к файлу протокола, в который упаковывается временные протоколы</param>
#endif
			void TransmitDebugReport(Common::Context* iContext, const FileSystem::path& iFileName);

//DOM-IGNORE-BEGIN

            /// Установить типы операторов для отладочного протоколирования
            void SetDebugReportTypes(const std::vector<ReplayUtilities::FunctionalityType>& iTypes);

            void OpenOperator(ReplayUtilities::TestOperatorPtr iOperator, ReplayUtilities::InputReplayDataPtr iInputData);
            void CloseOperator(ReplayUtilities::TestOperatorPtr iOperator, ReplayUtilities::OutputReplayDataPtr iOutputData);
            int GetCurrentLevel(int iThreadId);
            void SetLevel(int iThreadId, int iLevel);
            enum DebugReportMode{Input,InputOutput};
            void SetDebugReportMode(DebugReportMode iMode) { _mode=iMode; };
            DebugReportMode GetDebugReportMode() const { return _mode; };

		private:

			Common::Result Init(); // Инициализировать сессию
			Common::Result Finalize(); // Завершить сессию

			void AddBodies(unsigned int iCount, BodyPtr iBodies[], Common::Context* iContext); // Добавить тела в сессию
            void DeleteBodies(unsigned int iCount, BodyPtr iBodies[], Common::Context* iContext); // Удалить тела из сессии

            void AddAssemblies(unsigned int iCount, AssemblyPtr iAssemblies[], Common::Context* iContext); // Добавить сборки
            void UpdateAssemblies(Common::Context* iContext); // Обновить информацию о сборках
            void DeleteAssemblies(unsigned int iCount, AssemblyPtr iAssemblies[], Common::Context* iContext); // Удалить некорневые сборки из списка корневых сборок

            Common::Result AddJoint(Common::Context* iContext, const Constraints::JointPtr& iJoint); // Добавить сопряжения
            Common::Result DeleteJoints(Common::Context* iContext, const Constraints::JointArray& iJoints ); // Удалить сопряжения из сессии

            void SetLock(Common::Context* iContext); // Блокировать сессию из контекста
            void UnsetLock(Common::Context* iContext); // Разблокировать сессию из контекста

            template<class T> void Finalize(T* first);
            template <class T> void FinalizePtr(std::shared_ptr<T> first);

            void SynchronizeChanges (Common::Context* iContext); // Синхронизация изменений из контекста

            void PartUnlock(); // Разблокирование деталей в сессии.
            void _PartUnlock( const AssemblyPtr& iAssembly, std::set<AssemblyPtr>& ioProvenAssembly );

		private :
			Common::Context* _mainContext; // Основной контекст
			BodyPtr _firstBody; // Первое тело в двусвязанном списке тел в сессии
            AssemblyPtr _firstAssembly; // Первая сборка в списке корневых сборок
            Constraints::JointPtr _firstJoint; // Список сопряжений
            typedef std::set<Common::Context*> ContextSet;
            ContextSet _contexts; // Список контекстов в сессии
            omp_nest_lock_t _lock; // Синхронизация потоков
            ChangeLogPtr _changes; // Журнал изменений в сессии
            bool _journalling; // Вести журнал изменений

            double _linearPrecision; /// Линейная точность
            double _angularPrecision; /// Угловая точность
            double _unitPrecision; /// Относительная точность
            double _sizeBox; /// Максимально допустимые габариты модели
            unsigned int _maxThreads; /// Максимальное количество потоков

            std::unique_ptr<Common::Version> _version; /// Версия ядра

            bool _nonmanifold; // Режим работы с обобщённой топологией

            bool _useDebugReport; // Режим протоколирования
            int _debugReportLevel;
            std::map<int, bool> _threadToDebugReport;
            FileSystem::path _currentDebugPath;
            std::vector<ReplayUtilities::FunctionalityType> _debugReportTypes;
            std::map<int, int> _threadToLevel;
            DebugReportMode _mode;

            typedef std::map<Common::String, AttributeDefinitionPtr> AttributeDefinitions;
            AttributeDefinitions _attributes; /// Упорядоченный по именам список определений атрибутов
            unsigned int _attributesCounter; /// Механизм целочисленной идентификации определений атрибутов

            AttributeListPtr _attributesList; // Первый атрибут в списке

            bool _identify; /// Режим идентификации топологических элементов


            friend class Generators::TopologyModifier;
            friend class Generators::AssemblyManager;
            friend class Generators::BodyManager;
            friend class Common::Context;
            friend class Constraints::Joint;

            friend class Store::WriteModel;
            friend class Store::ReadModel;
			friend class Store::SAXReadModel;
//DOM-IGNORE-END
		};
	}
}
