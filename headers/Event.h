﻿// Event.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: RGK::Generators::Event
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "forward.h"
#include "decl.h"

namespace RGK
{
    namespace Generators
    {
#ifdef ENGLISH_HELP
        /// Types of events
        /// <seealso cref="Generators"/>
#else
        /// Типы событий
        /// <seealso cref="Генераторы"/>
#endif
        enum EventType
        {
#ifdef ENGLISH_HELP
			/// The time out event
#else
            /// Событие завершения временного интервала синхронизации
#endif
            TimeEventType, 
#ifdef ENGLISH_HELP
			/// The event of a creation of a topological item
#else
            /// Событие создания топологического элемента
#endif
            CreateTopolEventType,
#ifdef ENGLISH_HELP
			/// The event of a generation of triangles by a tesselator
#else
			/// Событие генерации треугольников сеточным генератором
#endif
			TesselationEventType,
        };

#ifdef ENGLISH_HELP
        /// <summary>The event for callback functions</summary>
		/// Different types of generators may have own classes are derived from Event for an enhancement of an information about the event
        /// <seealso cref="Generators"/>
#else
        /// <summary>Событие, которое передаётся в функцию обратного вызова</summary>
		/// В различных типах генераторов могут быть свои классы, порождённые из Event, для расширения информации о событии
        /// <seealso cref="Генераторы"/>
#endif
        class DLLEXPORT Event
        {
        public:
#ifdef ENGLISH_HELP
            /// Destructor
#else
            /// Деструктор
#endif
            virtual ~Event() {}

        public:
#ifdef ENGLISH_HELP
            /// Get the type of the event
            /// <returns>Type of the event</returns>
#else
            /// Получить тип события
            /// <returns>Тип события</returns>
#endif
            virtual EventType GetType() const = 0;

//DOM-IGNORE-BEGIN
        protected:

            /// <summary>Конструктор</summary>
            Event() {}

            friend class BaseGenerator;
//DOM-IGNORE-END
        };

#ifdef ENGLISH_HELP
        /// <summary>The "time is over" (timeout) event</summary>
#else
        /// <summary>Событие завершения временного интервала синхронизации</summary>
#endif
        class DLLEXPORT TimeEvent : public Event
        {
        public:

#ifdef ENGLISH_HELP
            /// Destructor
#else
            /// Деструктор
#endif
            virtual ~TimeEvent() {}

        public:
#ifdef ENGLISH_HELP
            /// Get the type of the event
            /// <returns>Type of the event</returns>
#else
            /// Получить тип события
            /// <returns>Тип события</returns>
#endif
            virtual EventType GetType() const { return TimeEventType; }

#ifdef ENGLISH_HELP
			/// Get the general performance time of a generator
			/// <returns>General time of a performance</returns>
#else
            /// Получить общее время выполнения генератора
            /// <returns>Общее время выполнения генератора</returns>
#endif
            double GetTime () const { return _time; }

//DOM-IGNORE-BEGIN
        protected:

            /// <summary>Конструктор</summary>
            /// <param name="iTime">Общее время выполнения генератора. Задаётся в секундах</param>
            TimeEvent(double iTime) : Event(), _time(iTime) {}

        private:
            double _time; /// Общее время выполнения генератора

            friend class BaseGenerator;
//DOM-IGNORE-END
        };

#ifdef ENGLISH_HELP
		/// <summary>The event of a creation of a topological item</summary>
#else
        /// <summary>Событие создания топологического элемента</summary>
#endif
        class DLLEXPORT CreateTopolEvent : public Event
        {
        public:

#ifdef ENGLISH_HELP
            /// Destructor
#else
            /// Деструктор
#endif
            virtual ~CreateTopolEvent() {}

        public:
#ifdef ENGLISH_HELP
			/// Get the type of the event
            /// <returns>Type of the event</returns>
#else
            /// Получить тип события
            /// <returns>Тип события</returns>
#endif
            virtual EventType GetType() const { return CreateTopolEventType; }

#ifdef ENGLISH_HELP
            /// Get the topological item is being created
            /// <returns>Topological item</returns>
#else
            /// Получить создаваемый топологический элемент
            /// <returns>Создаваемый топологический элемент</returns>
#endif
            Model::TopolPtr GetTopol () const { return _topol; }

//DOM-IGNORE-BEGIN
        protected:

            /// <summary>Конструктор</summary>
            /// <param name="iTopol">Создаваемый топологический элемент</param>
            CreateTopolEvent(const Model::TopolPtr& iTopol) : Event(), _topol(iTopol) {}

        private:
            Model::TopolPtr _topol; /// Создаваемый топологический элемент

            friend class TopologyModifier;
//DOM-IGNORE-END
        };

#ifdef ENGLISH_HELP
		/// <summary>The event of a generation of triangles by a tesselator
		/// It may be used for the interruption of mesh calculation process int the case of the maximum number of triangles threshold value overflowing</summary>
#else
		/// <summary>Событие генерации треугольников сеточным генератором.
		/// Может использоваться для прерывания процесса расчёта сетки при превышении порогового значения количества треугольников в сетке</summary>
#endif
        class DLLEXPORT TesselationEvent : public Event
        {
        public:

#ifdef ENGLISH_HELP
			/// Destructor
#else
            /// Деструктор
#endif
            virtual ~TesselationEvent() {}

        public:
#ifdef ENGLISH_HELP
			/// Get the type of the event
            /// <returns>Type of the event</returns>
#else
            /// Получить тип события
            /// <returns>Тип события</returns>
#endif
            virtual EventType GetType() const { return TesselationEventType; }

#ifdef ENGLISH_HELP
            /// Get the total count of the triangles 
            /// <returns>Count of triangles</returns>
#else
            /// Получить общее время количество треугольников
            /// <returns>Общее количество треугольников</returns>
#endif
            size_t GetTrianglesTotal () const { return _trianglesTotal; }

#ifdef ENGLISH_HELP
			/// Get the count of added triangles
            /// <returns> Count of triangles</returns>
#else
			/// Получить количество добавленных треугольников
            /// <returns> Количество добавленных треугольников</returns>
#endif
            size_t GetTriangleCount () const { return _trianglesCount; }

//DOM-IGNORE-BEGIN
        protected:

            /// <summary>Конструктор</summary>
            /// <param name="iTrianglesTotal"Общее количество треугольников</param>
			/// <param name="iTrianglesCount"Количество добавленных треугольников</param>
            TesselationEvent(size_t iTrianglesTotal, size_t iTrianglesCount) : Event(), _trianglesTotal(iTrianglesTotal), _trianglesCount(iTrianglesCount) {}

        private:
            size_t _trianglesTotal; /// Общее количество треугольников
			size_t _trianglesCount; /// Количество добавленных треугольников

            friend class BaseGenerator;
			friend class Tesselation::SurfaceTesselator;
//DOM-IGNORE-END
        };
    }
}

