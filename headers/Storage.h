﻿// Storage.h
//
// Данный файл является частью библиотеки классов 3D ядра RGK
// (c)Авторское право 2012-2014 АО "Топ Системы". Все права защищены.
//
// Контактная информация:
// mailto://rgk@topsystems.ru
// http://www.rgkernel.com/
//
/////////////////////////////////////////////////////////////////////////////
// Содержание файла: Описание класса RGK::Model::Storage
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include <vector>

#include "Common/String.h"
#include "decl.h"
#include "forward.h"
#include "Common/BaseTools.h"

namespace RGK
{
    namespace Model
    {
#ifdef ENGLISH_HELP
        /// <summary> Writing kernel data to and reading it from the data stream (a file)</summary>
        /// A class implementing writing kernel data to and reading it from the data stream (a file).
        /// Data can be represented in different formats.
#else
		/// <summary>Запись/чтение данных ядра в поток данных (файл)</summary>
		/// Класс обеспечивает запись и чтение данных ядра во внешний поток данных (файл). Данные могут представляться в различных форматах
#endif
        class DLLEXPORT Storage : public Common::BaseTools
        {
        public:
#ifdef ENGLISH_HELP
            /// A constructor
            /// <param name = "iContext">Model session context</param>
#else
			/// Конструктор
			/// <param name = "iContext">Контекст сессии модели</param>
#endif
            Storage(Common::Context* iContext);

#ifdef ENGLISH_HELP
            /// A destructor
#else
			/// Деструктор
#endif
            ~ Storage () {}

        public:

#ifdef ENGLISH_HELP
            /// Kernel data storage format
#else
            /// Формат хранения данных ядра
#endif
            enum Format
            {
#ifdef ENGLISH_HELP
                /// The XML format
#else
				/// Формат XML
#endif
                XML,
            };

#ifdef ENGLISH_HELP
            /// A class describing parameters of writing to a stream
#else
            /// Класс описания параметров записи в поток
#endif
            class DLLEXPORT WriteData
            {
            public:
#ifdef ENGLISH_HELP
                /// A default constructor
#else
				/// Конструктор по умолчанию
#endif
                WriteData();

#ifdef ENGLISH_HELP
                /// A constructor
                /// <param name = "stream">A stream to write data to</param>
                /// <param name = "format">Writing format</param>
                /// <param name = "module">The name of a module (program) performing the writing operation</param>
                /// <param name = "document">The name of a document (file) which the model data is written from</param>
                /// <param name = "saveinternaldata">A parameter setting (if it is necessary) to write internal data of model geometrical elements to a stream</param>
#else
				/// Конструктор
				/// <param name = "stream">Поток, в который необходимо сохранять данные</param>
				/// <param name = "format">Формат сохранения</param>
				/// <param name = "module">Название модуля (программы), выполняющей сохранение</param>
				/// <param name = "document">Название документа (файла), из которого сохраняются данные модели</param>
				/// <param name = "saveinternaldata">Параметр, отвечающий за необходимость сохранения в поток внутренних данных (представлений) геометрических элементов модели</param>
#endif
                WriteData(std::ostream* stream,Format format/*=XML*/, const Common::String& module, const Common::String& document, bool saveinternaldata/*=true*/);

#ifdef ENGLISH_HELP
                /// A destructor
#else
				/// Деструктор
#endif
				~ WriteData();

            public:

#ifdef ENGLISH_HELP
                /// Set the stream to write data to
#else
				/// Задать поток сохранения данных
#endif
                void SetStream(std::ostream* iStream) { _stream=iStream; }

#ifdef ENGLISH_HELP
                /// Set the name of the module (program) performing the writing operation
#else
				/// Задать имя модуля (программы), выполняющей сохранение данных
#endif
                void SetModuleName(const Common::String& iModule) { _module=iModule; }

#ifdef ENGLISH_HELP
                /// Set the name of the document (file) which the model data is written from
#else
				/// Задать название документа (файла), данные которого сохраняются в поток
#endif
                void SetDocumentName(const Common::String& iDocument) { _document=iDocument; }

#ifdef ENGLISH_HELP
                /// Add a part to write to a stream
#else
				/// Добавить тело для сохранения в поток
#endif
                void AddPart(const Model::PartPtr& part) { _parts.push_back(part); }

#ifdef ENGLISH_HELP
                /// Add a curve or a surface to write to a stream
#else
                /// Добавить кривую или поверхность для сохранения в поток
#endif
                void AddGeometry(const Geometry::GeometryPtr& iGeometry) { _geometry.push_back(iGeometry); }

#ifdef ENGLISH_HELP
                /// Add a constraint to write to a stream
#else
                /// Добавить сопряжение для сохранения в поток
#endif
                void AddJoint(const Constraints::JointPtr& iJoint) { _joints.push_back(iJoint); }

#ifdef ENGLISH_HELP
                /// Add an identifier to write to a stream
#else
                /// Добавить идентификатор для сохранения в поток
#endif
                void AddIdentifier(const Identification::IdentifierPtr& iIdentifier) { _identifiers.push_back(iIdentifier); }

#ifdef ENGLISH_HELP
                /// Set the data writing format
#else
				/// Задать формат сохранения данных
#endif
                void SetFormat(Format iFormat) { _format=iFormat; }

#ifdef ENGLISH_HELP
                /// Turn writing internal data of model geometrical elements to a stream on/off
#else
				/// Задать необходимость сохранения внутренних данных (представлений) геометрических элементов модели
#endif
                void SetSaveInternalData(bool iSaveInternaldata) { _representation = iSaveInternaldata; }

#ifdef ENGLISH_HELP
                /// Reserve an internal buffer for adding a given number of bodies
#else
				/// Зарезервировать внутренний буфер для добавления заданного числа тел
#endif
                void ReserveBuffer(int iBodies, int iGeometry=0) { if(iBodies>0)_parts.reserve(iBodies); if(iGeometry>0)_geometry.reserve(iGeometry); }

				/// Задать чтение пользовательских данных
				void SetUserDataWriter(const Interfaces::UserDataWriterPtr& iWriter) { _writer=iWriter; }

//DOM-IGNORE-BEGIN
            private:
                Model::PartArray _parts;
                Geometry::GeometryArray _geometry;
                Constraints::JointArray _joints;
                Identification::IdentifiersArray _identifiers;
                Common::String _document;
                Common::String _module;
				std::ostream* _stream;
                Format _format;
                bool _representation;
				Interfaces::UserDataWriterPtr _writer;

                friend class Storage;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
            /// Results of writing a model to a stream
#else
            /// Результаты сохранения модели в поток
#endif
            class DLLEXPORT WriteReport
            {
            public:
#ifdef ENGLISH_HELP
                /// A constructor
#else
				/// Конструктор
#endif
                WriteReport () {}

#ifdef ENGLISH_HELP
                /// A destructor
#else
				/// Деструктор
#endif
                ~WriteReport () {}

//DOM-IGNORE-BEGIN
            private:
                friend class Storage;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
            /// Write data
            /// <param name = "data">Parameters of writing to a stream</param>
            /// <param name = "report">Data writing report</param>
            /// <returns>Data writing result
            /// - Result::Success in case of successful execution</returns>
#else
			/// Выполнить сохранение данных
			/// <param name = "data">Описание параметров сохранения</param>
			/// <param name = "report">Отчёт о сохранении данных</param>
            /// <returns>Результат сохранения данных
			/// - Result:::Success в случае отсутствия ошибок</returns>
#endif
			Common::Result Write(const WriteData& data, WriteReport& report);

#ifdef ENGLISH_HELP
            /// A class describing parameters of reading from a stream
#else
            /// Класс описания параметров чтения данных из потока
#endif
            class DLLEXPORT ReadData
            {
            public:
#ifdef ENGLISH_HELP
                /// A default constructor
#else
				/// Конструктор по умолчанию
#endif
                ReadData();

#ifdef ENGLISH_HELP
                /// A constructor
                /// <param name = "stream">A stream to read data from</param>
                /// <param name = "format">Reading format</param>
                /// <param name = "readinternaldata">A parameter that setting (if it is necessary) to read internal data (representations)
                /// of model geometrical elements from a stream</param>
#else
				/// Конструктор
				/// <param name = "stream">Поток, из которого необходимо читать данные</param>
				/// <param name = "format">Формат сохранения</param>
				/// <param name = "readinternaldata">Параметр, отвечающий за необходимость чтения из потока внутренних данных (представлений) геометрических элементов модели</param>
#endif
                ReadData(std::istream* stream,Format format=XML,bool readinternaldata=true);
                
#ifdef ENGLISH_HELP
                /// A destructor
#else
				/// Деструктор
#endif
				~ ReadData();

            public:

#ifdef ENGLISH_HELP
                /// Set the stream to read data from
                /// <param name = "stream">A stream to read data from</param>
#else
				/// Установить поток чтения данных
				/// <param name = "stream">Поток, из которого необходимо читать данные</param>
#endif
                void SetStream(std::istream* stream) { _stream=stream; }

#ifdef ENGLISH_HELP
                /// Set the data representation format
                /// <param name = "format">Data representation format</param>
#else
				/// Задать формат представления данных
                /// <param name = "format">Формат представления данных</param>
#endif
                void SetFormat(Format format) { _format=format; }

#ifdef ENGLISH_HELP
                /// Turn reading internal data (representations) of model geometrical elements to a stream on/off
                /// <param name = "readinternaldata">A parameter setting (if it is necessary) to read internal data (representations)
                /// of model geometrical elements from a stream</param>
#else
				/// Задать необходимость чтения внутренних данных (представлений) геометрических элементов модели
				/// <param name = "readinternaldata">Параметр, отвечающий за необходимость чтения из потока внутренних данных (представлений) геометрических элементов модели</param>
#endif
                void SetReadInternalData(bool readinternaldata) { _representation = readinternaldata; }

				/// Задать чтение пользовательских данных. Задана структура пользовательских данных
				void SetUserDataStructure(const Store::UserDataStructurePtr& iStructure) { _structure=iStructure; }

				/// Задать чтение пользовательских данных, если данные читаются одним куском
				void SetUserDataReader(const Interfaces::UserDataReaderPtr& iReader);

//DOM-IGNORE-BEGIN
            private:
				std::istream* _stream;
                Format _format;
                bool _representation;
				Store::UserDataStructurePtr _structure;

                friend class Storage;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
            /// Results of reading model data from a stream
#else
            /// Результаты чтения данных модели из потока
#endif
            class DLLEXPORT ReadReport
            {
            public:
#ifdef ENGLISH_HELP
                /// A constructor
#else
				/// Конструктор
#endif
                ReadReport ();
#ifdef ENGLISH_HELP
                /// A destructor
#else
				/// Деструктор
#endif
                ~ReadReport ();

            public:
#ifdef ENGLISH_HELP
                /// Get the name of the module (program) which was used for writing data to a stream
                /// <returns>The name of the module (program) which was used for writing data to a stream</returns>
#else
				/// Получить название модуля (программы), который использовался для сохранение данных в поток
    			/// <returns>Название модуля (программы), который использовался для сохранение данных в поток</returns>
#endif
                const Common::String& GetModuleName() const { return _module; }

#ifdef ENGLISH_HELP
                /// Get the name of the document (file) which was stored int a stream
                /// <returns>The name of the document (file) which was stored in a stream</returns>
#else
				/// Получить название документа, который был сохранён в поток
    			/// <returns>Название документа, который был сохранён в поток</returns>
#endif
                const Common::String& GetDocumentName() const { return _document; }

#ifdef ENGLISH_HELP
                /// Get the list of parts which have been read from a stream.
                /// <returns>The list of parts which have been read from a stream</returns>
#else
				/// Получить список тел, прочитанных из потока. Порядок тел сохраняется согласно порядку при сохранении.
    			/// <returns>Список  тел, прочитанных из потока</returns>
#endif
                const Model::PartArray& GetParts() const { return _parts; }

#ifdef ENGLISH_HELP
                /// Get the list of curves and surfaces which have been read from a stream. The order of curves and surfaces is preserved
                /// the same, as it was, when the data was written.
                /// <returns>The list of curves and surfaces which have been read from a stream</returns>
#else
                /// Получить список кривых и поверхностей, прочитанных из потока. Порядок кривых и поверхностей сохраняется согласно порядку при сохранении.
    			/// <returns>Список  кривых и поверхностей, прочитанных из потока</returns>
#endif
                const Geometry::GeometryArray& GetGeometry() const { return _geometry; }

#ifdef ENGLISH_HELP
                /// Get the list of identifiers which have been read from a stream. The order of identifiers is preserved
                /// the same, as it was, when the data was written.
                /// <returns>The list of identifiers which have been read from a stream</returns>
#else
                /// Получить список идентификаторов, прочитанных из потока. Порядок идентификаторов сохраняется согласно порядку при сохранении.
    			/// <returns>Список идентификаторов, прочитанных из потока</returns>
#endif
                const Identification::IdentifiersArray& GetIdentifiers() const { return _identifiers; }

#ifdef ENGLISH_HELP
                /// Get the kernel version which was used when the data was written to a stream
                /// <returns>The kernel version which was used when the data was written to a stream</returns>
#else
				/// Получить версию ядра, которая была использована при сохранении данных в поток
    			/// <returns>Версия ядра, которая была использована при сохранении данных в поток</returns>
#endif
                void GetProductVersion(unsigned int version[4]) const;

#ifdef ENGLISH_HELP
                /// Get the version of the file (stream) which has been read
                /// <returns>The version of the file (stream) which has been read</returns>
#else
				/// Получить версию файла (потока), который был прочитан
    			/// <returns>Версия файла (потока), который был прочитан</returns>
#endif
                void GetFileVersion(unsigned int version[2]) const;

//DOM-IGNORE-BEGIN
            private:
                Common::String _document;
                Common::String _module;
                Model::PartArray _parts;
                Geometry::GeometryArray _geometry;
                Identification::IdentifiersArray _identifiers;
                unsigned int _productVersion[4];
                unsigned int _fileVersion[2];

                friend class Storage;
//DOM-IGNORE-END
            };

#ifdef ENGLISH_HELP
            /// Read data from a stream
            /// <param name = "data">Parameters of reading from a stream</param>
            /// <param name = "report">Data reading report</param>
            /// <returns>Data reading result
            /// - Result::Success in case of successful execution</returns>
#else
			/// Выполнить чтение данных из потока
			/// <param name = "data">Описание параметров чтения</param>
			/// <param name = "report">Отчёт о чтении данных</param>
			/// <returns>Результат чтения данных
			/// - Result:::Success в случае отсутствия ошибок</returns>
#endif
            Common::Result Read(const ReadData& data, ReadReport& report);
        };
    }
}
