#  RGK/Generators

---
**AssemblyManager.h**, **BaseGenerator.h**, **BodyConstructor.h**, **BodyManager.h**, **BodyTransformer.h**,
**Boolean.h**, **ChangeFaces.h**, **Deform.h**, **DivisionFaces.h**, **EdgeBlend.h**, **EulerOperators.h**, **Event.h**,
**Extrusion.h**, **FaceBlend.h**, **Faceter.h**, **FillHoles.h**, **GeometryConverter.h**, **Hollow.h**, **Imprint.h**,
**ImprintIsocline.h**, **InterruptionInfo.h**, **Law.h**, **Loft.h**, **MultipleGuideSweep.h**, **Offset.h**,
**PrimitiveGenerator.h**, **Projection.h**, **ReplaceFace.h**, **Rotation.h**, **Sewing.h**, **SheetBodyCreator.h**,
**SpringGenerator.h**, **Sweep.h**, **Taper.h**, **Thicken.h**, **ThreeFaceBlend.h**, **TopologyMap.h**,
**TopologyModifier.h**, **WireBodyCreator.h**, 

Генераторы используются в модели для создания тел при помощи различных методов и их модификации. Базовый класс 
генераторов содержит: версию генератора, монитор для отслеживания процесса выполнения генератора, 
разные временные отсечки, связанные с работой генератора, информация о причине прерывания генератора, точность 
построений, управление режимами идентификации топологических элементов и вывода подробной информации в класс Report 
генератора, а также уникальный id генератора. 

Доступные генераторы: 

  - генератор сборки
  - генератор тел
  - генератор управления телами модели
  - генератор трансформации тел
  - генератор булевых операций
  - генератор операции модификации граней 
  - - аффинные преобразования
  - - удаление 
  - - - простое
  - - - с расширением смежных граней
  - - - с построением поверхности затягивания
  - - замена поверхности нескольких граней, изменение радиуса сглаживания
  - генератор деформаций тела 
  - - сгибание
  - - деформация по кривой
  - - деформация по поверхности
  - - растяжение и скручивание
  - - скульптурная деформация
  - - деформация перекоса
  - генератор операции отделения граней
  - генератор операции сглаживания рёбер 
  - - сглаживание катящимся шаром 
  - - - постоянного радиуса
  - - - с радиусом заданным законом изменения
  - - - с радиусом заданным набором точек и радиусами в них 
  - - фаска 
  - - - с постоянными смещениями 
  - - - смещениями, заданными законами изменения
  - - - смещениями, заданными набором точек и смещениями в них
  - - сглаживание квадратичной формы с переменными смещениями и коэффициентом выпуклости формы, задаваемыми законом изменения или двумя наборами точек 
  - генератор операций Эйлера
  - генератор операции выталкивания (extrusion),
  - генератор операции сглаживания граней 
  - - сглаживание катящимся шаром постоянного радиуса
  - - - сглаживаемая грань обрезается пограням тела
  - - - по длинному опорному ребру 
  - - - по короткому опорному ребру
  - - - не обрезается
  - генератор операции заполнения отверстия в оболочке листового тела гранью 
  - - отверстие заполняется поверхностью затягивания
  - - плоскостью
  - - поверхностью затягивания
  - - с заданием рёбер, на которых не требуется непрерывность первой производной
  - - продолжением граней
  - генератор конвертации геометрии топологических элементов (грани и ребра)
  - генератор операции построения тонкостенных и эквидистантных тел
  - генератор операции разрезания набора граней заданной кривой
  - генератор операции построения изоклин-кривых и их встраивание в топологию тела
  - генератор операции построения тела по набору сечений (loft)
  - генератор операции кинематическое тело по двум направляющим
  - генератор операции построения эквидистантного тела
  - генератор примитивов
  - генератор проекции топологических элементов
  - генератор операции замены набора граней листовым телом
  - генератор операции вращения
  - генератор операции сшивка (сшиваются листовые тела)
  - генератор операции построения листового тела 
  - - по набору граней
  - - по поверхности
  - - по оболочке
  - - по поверхности
  - - ограниченной прямоугольной областью в UV-пространстве поверхности
  - генератор операции создания спиралей и пружин
  - генератор операции "По траектории"
  - генератор операции построения уклона
  - генератор операции придания толщины листовому телу
  - генератор операции трёхгранного сглаживания
  - генератор операции построения проволочного тела

---
***Event*** - базовый класс, описывающий "событие", которое передаётся в функцию обратного вызова. Доступные 
типы событий: событие завершения временного интервала синхронизации, событие создания топологического элемента, событие 
генерации треугольников сеточным генератором.

---
В данных файлах, как я понял, реализован вспомогательный функционал.

**Boolean/BooleanTracer.h**, **Boolean/IntersectionSegmentSeries.h**,

**Extrusion/ExtrusionUtils.h**, 

**Imprint/CuttingPeriodicFace.h**

**IntersectionGraph/IntersectionAssocContainers.h**, **IntersectionGraph/IntersectionGraph.h**, 
**IntersectionGraph/IntersectionGraphBuilder.h**, **IntersectionGraph/IntersectionPoint.h**, 
**IntersectionGraph/IntersectionSegment.h**, **IntersectionGraph/Triplet.h**,

**Projection/EllipsesIntersection.h**, 
