#  RGK/Common

**BaseTools.h**, **CacheData.h**, **Context.h**, **DoubleLink.h**, **ErrorReport.h**, **FacetParameters.h**,
**FlagSet.h**, **OmpLock.h**, **ReferenceCounting.h**, **Result.h**, **Statistics.h**, **Stream.h**, **String.h**,
**Timer.h**, **Version.h**, 

---
***RGK::Common::CacheData*** - информация для управления кэшированием данных: 

  * частота использования
  * размер геометрии
  * последний запрос

---
***RGK::Common::Context*** - контекст передаётся всем функциям, работающим с моделью, контекст используется для решения следующих 
задач: 

  * протоколирование изменений в модели
  * синхронизация параллельных вычислений
  * доступ к активному набору данных(сессии) ядра
  * получение параметров точности и других настроек ядра

---
**detail/codecvt.h**, **detail/put_time.h**, **detail/wstring_convert.h**, **detail/yvals.h**
