#  RGK/Math

**AffineMap3D.h**, **Axis3D.h**, **BoundingBox.h**, **FpClassify.h**, **LCS3D.h**, **MathTypes.h**,
**ProjectiveMap3D.h**, **Rectangle.h**, **Vector2D.h**, **Vector3D.h**, **Vector4D.h**,

Набор самых базовых объектов.

---
**3D/GeometryUtils.h**, 

Набор вспомогательных рассчётных и проверочных функций.

---
**Integration/GaussKronrodIntegration.h**, **Integration/GaussLegendreIntegration.h**,

Функции численного интегрирования.

---
**LinearAlgebra/CompleteOrthogonalFactorization.h**, **LinearAlgebra/FactorizationRRQR.h**,
**LinearAlgebra/LinearSolver.h**, **LinearAlgebra/LinearTransformation.h**,
**LinearAlgebra/LinearTransformationUtils.h**, **LinearAlgebra/MatrixFactorization.h**, **LinearAlgebra/Permutation.h**,
**LinearAlgebra/PlacedMatrix.h**, **LinearAlgebra/PlacedVector.h**, **LinearAlgebra/PseudoInverseSolver.h**,
**LinearAlgebra/SimpleMatrix.h**, **LinearAlgebra/SymmetricEigenFactorization.h**,
**LinearAlgebra/TemplateLinearSolver.h**, **LinearAlgebra/TriangularSolver.h**, **LinearAlgebra/VectorOperations.h**,

Методы линейной алгебры: решения СЛАУ, разложения матриц и различные их преобразования. Код почти незадокументирован. 
API очень сырое.

---
**MathLib/instrset.h**, **MathLib/instrset_detect.h**, **MathLib/macros.h**, **MathLib/matrix2d_fpu.h**,
**MathLib/matrix2d_sse.h**, **MathLib/matrix3d_fpu.h**, **MathLib/matrix3d_sse.h**, **MathLib/matrix4d_fpu.h**,
**MathLib/matrix4d_sse.h**, **MathLib/tfmdefs.h**, **MathLib/tfmparam.h**, **MathLib/tfmtype.h**,
**MathLib/vector2d_fpu.h**, **MathLib/vector2d_sse.h**, **MathLib/vector3d_fpu.h**, **MathLib/vector3d_sse.h**, 
**MathLib/Quaternion_fpu.h**,

Судя по всему тут содержатся какие-то оптимизации для работы с базовыми объектами. Но если честно, я мало что понял:
код очень грязный и никак не документорован. Из интересного - тут есть кватернионы.

---
**NewtonSolver/GradientFunction.h**, **NewtonSolver/IntersectionWidthCalculator.h**, 
**NewtonSolver/ManifoldBasedEquationSystem.h**, **NewtonSolver/NewtonRaphsonSolver.h**, 
**NewtonSolver/NonlinearFunction.h**, **NewtonSolver/PredictorCorrector.h**, **NewtonSolver/RungeKuttaSolver.h**,
**NewtonSolver/SmoothnessChecks.h**, **NewtonSolver/Taylor.h**, **NewtonSolver/TraceMode.h**,
**NewtonSolver/VariablesScaling.h**,

Методы решения систем нелинейных уравнений. Документации почти нет. API очень сырое.Из интересного: разложение в ряд 
Тейлора, масштабирование переменных системы уравнений, система уравнений для задачи пересечения многообразий (manifold).

---
**NURBS/ArcLengthParameterisationCurve.h**, **NURBS/LoftSurfaceBuilder.h**, **NURBS/MultipleGuideSweepBuilder.h**,
**NURBS/NURBSApproximationBuilder.h**, **NURBS/NURBSBuilder.h**, **NURBS/NURBSExtensionBuilder.h**,
**NURBS/NURBSInterpolationBuilder.h**, **NURBS/NURBSRecognizer.h**, **NURBS/NURBSUtils.h**,
**NURBS/SweepSurfaceBuilder.h**,

Доступно очень большое разнообразие различных операций по редактированию (соединение/разделение кривых и поверхностей, 
добавление узлов, изменение порядка, различные интерполяции и т.п.) с NURBS-кривыми и поверхностями; создание большого 
разнообразия аналитических кривых и поверхностей в виде NURBS; распознование того, является ли NURBS аналитической 
кривой или поверхностью и определение типа этой аналитической кривой (поверхности); расширение NURBS-кривых и 
поверхностей: линейное, непрерывное по кривизне, натуральное и по дуге окружности.

---
**PointInclusion/FaceBoundaryBuilder.h**, **PointInclusion/FaceInclusionChecker.h**, **PointInclusion/LocationEnum.h**, 
**PointInclusion/PoleDetector.h**

Набор функций для проверки положения точки относительно топологического объекта (например, проверка того, принадлежит ли 
точка грани).

