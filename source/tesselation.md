#  Tesselation

**TesselationDefines.h**, **TesselationSettings.h**, **SurfaceTesselator.h**, 

---
**Surface/Mesh/Iterator.h**, **Surface/Mesh/Constants.h**, **Surface/Mesh/SurfaceMesh.h**, **Surface/Mesh/Defines.h**,
**Surface/Mesh/Index.h**, **Surface/Mesh/Interval.h**, **Surface/Mesh/Spin.h**, 

***RGK::Mesh::SurfaceMesh*** - класс для хранения триангуляции поверхности. Очень много функций. В том числе есть 
функции редактирования топологии.

---
**Debug/TesselationDebug.h**

В папке **Debug** задаются настройки выдачи диагностической информации триангулятором.
