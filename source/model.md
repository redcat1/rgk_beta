#  RGK/Model


**EntityIterator.h, Session.h, SessionState.h, Storage.h, Synchronization.h**

---
***Synchronization*** - класс, обеспечивающий синхронизацию для работы с телами в многопоточном режиме.

---
***Session*** - Сессия ядра хранит изолированный набор системных установок (параметров точности вычислений, максимальных
габаритов и т.д.) и все тела, созданные в ней. Любой объект модели принадлежит определённой сессии. При удалении сессии
все объекты модели, принадлежащие ей, автоматически удаляются.
Хранит в себе 3 типа точностей: линейная, угловая и относительная. Набор сборок, набор тел, ограничения. Журнал
изменений в сессии. Атрибуты.

---
***Storage*** - Как гласит описание: Класс обеспечивает запись и чтение данных ядра во внешний поток данных (файл). 
Данные могут представляться в различных форматах. Из форматов доступен только **XML**. В классе определена структура
данных ***WriteData***, хранящая по отдельности массивы тел, кривых с поверхностями, сопряжений и идентификаторов для
сохранения в поток. Запись осуществляется методом ***Common::Result Write(const WriteData& data, WriteReport& report)***.
Аналогично метод ***Common::Result Read(const ReadData& data, ReadReport& report)*** производит чтение данных из потока.
При этом мне видится некоторая ассиметрия: Записываемые данные хранятся в ***WriteData***, а прочитанные в 
***ReadReport***. Вообще я лично не очень понял смысл разделения на XXXData и XXXReport. Также я заметил, что записать
ограничения можно, а прочитать нельзя. Как это понимать не знаю. Также декларируется возможность "*сохранения внутренних
данных (представлений) геометрических элементов модели*".


---
**Attributes/Attribute.h, Attributes/AttributeDefinition.h, Attributes/BinaryAttribute.h,**
**Attributes/CompositeAttribute.h, Attributes/DoubleArrayAttribute.h, Attributes/DoubleAttribute.h,**
**Attributes/IntArrayAttribute.h, Attributes/IntAttribute.h, Attributes/StringAttribute.h**

---
**Atributes** - Атрибуты предоставляют возможность работать с бинарными данных, числами (целыми и с плавающей точкой) и
их массивами, а также строками.


---
**Topols/Assembly.h, Topols/Body.h, Topols/CoEdge.h, Topols/CoFace.h, Topols/Edge.h, Topols/Face.h, Topols/Loop.h,**
**Topols/Part.h, Topols/PartInstance.h, Topols/PartInstancePath.h, Topols/Region.h, Topols/Shell.h, Topols/Topol.h,**
**Topols/TopolPath.h, Topols/Vertex.h**

---
***Topol*** - Базовый абстрактный класс, представляющий объект модели. 

**Определяет следующие топологические типы:**

  - вершина(vertex)
  - ребро (edge)
  - R-ребро (co-edge)
  - грань (face)
  - R-грань (co-face)
  - цикл (loop)
  - регион (region)
  - оболочка (shell)
  - тело (body)
  - деталь (part)
  - экземпляр (part instance)
  - сборка (assembly)
  - также под дефайном есть тип R-ребро сетки (mesh co-edge). 

Так же класс хранит 2 идентификатора: идентификатор элемента, прочитанного из хранилища и **Identification::IdentifierPtr**.

---
***Vertex*** - содержит слабые ссылки на ребра и тело, которым принадлежит вершина, а также на цикл, который першина 
порождает

---
***Edge*** - содержит слабые ссылки на своё тело, R-ребро; 2 точности (_tolerance и _realTolerance), но почему-то 
методы, позволяющие управлять точностями - приватные. В классе определено большое разнообразие типов и типов выпуклости
ребра. Методы, позволяющие узнать топологические отношения между ребром и другими ребрами, циклами и гранями.

---
***CoEdge*** - класс описывает параметры использования (подключения, вхождения) ребра (RGK::Model::Edge) в каждой из 
смежных граней (RGK::Model::Face), на пересечении которых находится это ребро.

---
***Face*** - грань устроена по тому же принципу, что и ребро: хранит слабые ссылки на тело и R-грани, состоит из циклов 
и поверхности, предоставляет интерфейс для работы с топологией, доступно BSpline-представление.

---
***CoFace*** - класс описывает параметры использования (подключения, вхождения) грани (RGK::Model::Face) в каждой из 
оболочек (RGK::Model::Shell), на пересечении которых находится эта грань.

---
***Loop***  - цикл принадлежит грани и состоит из набора R-рёбер. Имеет хорошую типизацию: цикл, сформированный 
единственной вершиной, проволочный цикл грани твёрдого или листового тела, внешний цикл грани твёрдого или листового 
тела, внутренний цикл грани твёрдого или листового тела. А в остальном по своей идеологии ничем не отличается от грани
 или ребра.
 
 ---
***Region*** - тело состоит из одного или более регионов. Регион может представлять собой: замкнутую область 
(твердотельный регион) (Region::Type::Solid), замкнутую полость твёрдого тела (Region::Type::Void). Если я правильно
понял, это аналог нашего ***MbFaceShell***.

---
***Shell*** - оболочка. Тело состоит из регионов, каждый из которых состоит из одной или более оболочек. Оболочка 
принадлежит телу и региона и состоит из R-граней.

---
***Body*** - представление тела в модели. Доступные типы тел: пустое (Empty), минимальное (Acorn, оболочки составляются 
из одной вершины), проволочное тело (Wire), листовое тело (Sheet), твёрдое тело (Solid). Тип **Body*** хранит в себе 
области (region), оболочки, грани, ребра и вершины. Из интересного: доступна возможность получить представление для 
набора граней, где каждая грань представлена необрезанной поверхностью в виде кусочного Безье.

---
***Part*** - базовый класс детали и подсборки. Хранит синхронизацию и свою сессию.

---
***PartInstance*** - класс экземпляра элемента сборки. Используется для описания положения деталей и подсборок в 
сборках. Данный класс хранит ссылку на объект (либо подсборка следующего уровня, либо тело), а также карту 
преобразования, с которой данный объект входит в сборку.

---
***PartInstancePath*** - связанный путь от сборки верхнего уровня к подсборкам или телам.

---
***Assembly*** - сборка. Состоит из экземпляров (part instance).
