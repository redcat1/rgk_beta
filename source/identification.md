#  RGK/Identification

**BooleanIdentifiers.h**, **CommonIdentifiers.h**, **DescendantIdentifier.h**, **ExtrusionIdentifiers.h**, 
**IdentificationTools.h**, **Identifier.h**, **LoftIdentifiers.h**, **MultipleGuideSweepIdentifier.h**,
**PrimitiveGeneratorIdentifiers.h**, **RotationIdentifiers.h**, **SheetBodyCreatorIdentifiers.h**, 
**SweepIdentifiers.h**, **ThickenIdentifiers.h**, **UserDataIdentifier.h**

Тут описаны разные идентификаторы. У каждого типа топологического объекта и операции есть свой класс-идентификатор. 
Базовый класс ***RGK::Identification::Identifier***. Любой идентификатор имеет свой тип, id операции и id хранения в 
файле.
