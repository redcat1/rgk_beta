import os
import codecs


def mddecor(decfunc):
    def wrapper(path, mdtag):
        items = list()
        isfile, isdir = False, False
        for item in decfunc(path, mdtag):
            items.append(mdtag+item+mdtag)
            if os.sep in item:
                isdir = True
            elif os.path.isfile(path+os.sep+item):
                isfile = True
        print('### '+os.path.basename(path)+'\n')
        if isfile and isdir:    
            print('**<ins>Список файлов и папок:</ins>**')
        elif isfile:
            print('**<ins>Список файлов:</ins>**')
        elif isdir:
            print('**<ins>Список папок:</ins>**')
        l = ', '.join(items)
        if len(l) > 120:
            l0, l = l, ''
            ind = l0.find(',', 120) + 1
            while ind > 0:
                ind0 = l0[: ind-1].rfind(',') + 1
                if ind0 > 0:
                    l += l0[: ind0] + '\n'
                    l0 = l0[ind0:].strip()
                else:
                    l += l0[: ind] + '\n'
                    l0 = l0[ind:].strip()
                ind = l0.find(',', 120) + 1
            l += l0 
        print(l)
    return wrapper
        

@mddecor        
def print_all_dirs(path, mdtag):
    if os.path.exists(path) and os.path.isdir(path):
        for item in sorted(os.listdir(path)): 
            if os.path.isdir(path+os.sep+item): 
                yield item
        

@mddecor
def print_all_files(path, mdtag):
    if os.path.exists(path) and os.path.isdir(path):
        for item in sorted(os.listdir(path)):
            if os.path.isfile(path+os.sep+item):
                yield item 


@mddecor
def list_of_files(path, mdtag=''):
    pathlen = len(path)+1
    for dirpath, dirnames, filenames in sorted(os.walk(path), key=lambda item: item[0]):
        items = list()
        for fname in sorted(filenames):
            if fname.endswith('.cpp') or fname.endswith('.h') or fname.endswith('.hpp'):
                item = dirpath + os.sep + fname
                yield item[pathlen:]


if __name__ == '__main__':
    path = '/home/alakhverdyants/Projects/3rdparty/rgp_sdk_gnu/Include/RGK/Common'
    list_of_files(path, '**')
    print_all_dirs(path, '**')
    print_all_files(path, '**')
    print('ok')
